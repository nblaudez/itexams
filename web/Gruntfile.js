module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: '<json:package.json>',

        cssmin: {
            css:{
                src: 'css/ct_styles.css',
                dest: 'css/ct_styles.min.css'
            }
        },
        uglify: {},

        less: {
            production: {
                files: {
                    "css/ct_styles.css": "less/styles.less"
                }
            }
        }
    });



    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-less');

    // Default task.
    grunt.registerTask('default', ['less','cssmin']);

};
