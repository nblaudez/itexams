// Commom Plugins
(function($) {

    'use strict';

    // Scroll to Top Button.
    if (typeof jsTheme.PluginScrollToTop !== 'undefined') {
        jsTheme.PluginScrollToTop.initialize();
    }

    // Tooltips
    if ($.isFunction($.fn['tooltip'])) {
        $('[data-tooltip]:not(.manual), [data-plugin-tooltip]:not(.manual)').tooltip();
    }

    // Popover
    if ($.isFunction($.fn['popover'])) {
        $(function() {
            $('[data-plugin-popover]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.popover(opts);
            });
        });
    }

    // Validations
    if (typeof jsTheme.PluginValidation !== 'undefined') {
        jsTheme.PluginValidation.initialize();
    }

    // Parallax
    if (typeof jsTheme.PluginParallax !== 'undefined') {
        jsTheme.PluginParallax.initialize();
    }

    // Match Height
    if ($.isFunction($.fn['matchHeight'])) {

        $('.match-height').matchHeight();

        // Featured Boxes
        $('.featured-boxes .featured-box').matchHeight();

        // Featured Box Full
        $('.featured-box-full').matchHeight();

    }

}).apply(this, [jQuery]);

// Animate
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginAnimate'])) {

        $(function() {
            $('[data-plugin-animate], [data-appear-animation]').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginAnimate(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Carousel
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginCarousel'])) {

        $(function() {
            $('[data-plugin-carousel]:not(.manual), .owl-carousel:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginCarousel(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Chart.Circular
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginChartCircular'])) {

        $(function() {
            $('[data-plugin-chart-circular]:not(.manual), .circular-bar-chart:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginChartCircular(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Counter
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginCounter'])) {

        $(function() {
            $('[data-plugin-counter]:not(.manual), .counters [data-to]').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginCounter(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Lazy Load
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginLazyLoad'])) {

        $(function() {
            $('[data-plugin-lazyload]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginLazyLoad(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Lightbox
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginLightbox'])) {

        $(function() {
            $('[data-plugin-lightbox]:not(.manual), .lightbox:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginLightbox(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Masonry
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginMasonry'])) {

        $(function() {
            $('[data-plugin-masonry]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginMasonry(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Match Height
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginMatchHeight'])) {

        $(function() {
            $('[data-plugin-match-height]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginMatchHeight(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Progress Bar
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginProgressBar'])) {

        $(function() {
            $('[data-plugin-progress-bar]:not(.manual), [data-appear-progress-animation]').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginProgressBar(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Revolution Slider
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginRevolutionSlider'])) {

        $(function() {
            $('[data-plugin-revolution-slider]:not(.manual), .slider-container .slider:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginRevolutionSlider(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Sort
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginSort'])) {

        $(function() {
            $('[data-plugin-sort]:not(.manual), .sort-source:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginSort(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Sticky
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginSticky'])) {

        $(function() {
            $('[data-plugin-sticky]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginSticky(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Toggle
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginToggle'])) {

        $(function() {
            $('[data-plugin-toggle]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginToggle(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Tweets
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginTweets'])) {

        $(function() {
            $('[data-plugin-tweets]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginTweets(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Video Background
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginVideoBackground'])) {

        $(function() {
            $('[data-plugin-video-background]:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginVideoBackground(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Word Rotate
(function($) {

    'use strict';

    if ($.isFunction($.fn['jsThemePluginWordRotate'])) {

        $(function() {
            $('[data-plugin-word-rotate]:not(.manual), .word-rotate:not(.manual)').each(function() {
                var $this = $(this),
                    opts;

                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;

                $this.jsThemePluginWordRotate(opts);
            });
        });

    }

}).apply(this, [jQuery]);

// Commom Partials
(function($) {

    'use strict';

    // Sticky Header
    if (typeof jsTheme.StickyHeader !== 'undefined') {
        jsTheme.StickyHeader.initialize();
    }

    // Nav Menu
    if (typeof jsTheme.Nav !== 'undefined') {
        jsTheme.Nav.initialize();
    }

    // Search
    if (typeof jsTheme.Search !== 'undefined') {
        jsTheme.Search.initialize();
    }

    // Newsletter
    if (typeof jsTheme.Newsletter !== 'undefined') {
        jsTheme.Newsletter.initialize();
    }

    // Account
    if (typeof jsTheme.Account !== 'undefined') {
        jsTheme.Account.initialize();
    }




}).apply(this, [jQuery]);