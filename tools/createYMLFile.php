<?php

function gen_slug($str){
    # special accents
    $a = array('À','Á','Â','Ã','Ä','Å','Æ','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ð','Ñ','Ò','Ó','Ô','Õ','Ö','Ø','Ù','Ú','Û','Ü','Ý','ß','à','á','â','ã','ä','å','æ','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ø','ù','ú','û','ü','ý','ÿ','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','Ð','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','?','?','J','j','K','k','L','l','L','l','L','l','?','?','L','l','N','n','N','n','N','n','?','O','o','O','o','O','o','Œ','œ','R','r','R','r','R','r','S','s','S','s','S','s','Š','š','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Ÿ','Z','z','Z','z','Ž','ž','?','ƒ','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','?','?','?','?','?','?');
    $b = array('A','A','A','A','A','A','AE','C','E','E','E','E','I','I','I','I','D','N','O','O','O','O','O','O','U','U','U','U','Y','s','a','a','a','a','a','a','ae','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','o','u','u','u','u','y','y','A','a','A','a','A','a','C','c','C','c','C','c','C','c','D','d','D','d','E','e','E','e','E','e','E','e','E','e','G','g','G','g','G','g','G','g','H','h','H','h','I','i','I','i','I','i','I','i','I','i','IJ','ij','J','j','K','k','L','l','L','l','L','l','L','l','l','l','N','n','N','n','N','n','n','O','o','O','o','O','o','OE','oe','R','r','R','r','R','r','S','s','S','s','S','s','S','s','T','t','T','t','T','t','U','u','U','u','U','u','U','u','U','u','U','u','W','w','Y','y','Y','Z','z','Z','z','Z','z','s','f','O','o','U','u','A','a','I','i','O','o','U','u','U','u','U','u','U','u','U','u','A','a','AE','ae','O','o');
    $slug= strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/','/[ -]+/','/^-|-$/'),array('','-',''),str_replace($a,$b,$str)));
    $slug = str_replace(array("\r", "\n"), '', $slug);
    return $slug;
}

/* Connexion à une base ODBC avec l'invocation de pilote */
$dsn = 'mysql:dbname=quarkcenter;host=127.0.0.1';
$user = 'root';
$password = 'nico040677';

try {
    $db = new PDO($dsn, $user, $password);
    $sql="SELECT * FROM categories";
    $i=1;
    foreach  ($db->query($sql, PDO::FETCH_ASSOC) as $row) {
        $category_name=gen_slug($row["name"]);
        $query="SELECT * FROM questions where category_id=".$row["id"];
        $i=1;
        foreach($db->query($query, PDO::FETCH_ASSOC) as $row2) {
            mkdir("php/".$category_name."/".$i."/answers",null,true);
            file_put_contents("php/$category_name/$i/question.txt",$row2["question"]);


            $query="SELECT * FROM questions_response WHERE question_id=".$row2["id"];
            $j=1;
            foreach($db->query($query, PDO::FETCH_ASSOC) as $row3) {
                $query="SELECT * FROM questions_good_response WHERE question_id=".$row2["id"];
                $result=$db->query($query, PDO::FETCH_ASSOC)->fetch();

                if($result["questions_response_id"]==$row3["id"]) {
                    file_put_contents("php/$category_name/$i/answers/{$j}_true.txt",$row3["response"]);
                } else {
                    file_put_contents("php/$category_name/$i/answers/{$j}_false.txt",$row3["response"]);
                }
                $j++;
            }
            $i++;
        }
    }
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
}