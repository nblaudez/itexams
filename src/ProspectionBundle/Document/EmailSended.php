<?php
namespace ProspectionBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class EmailSended
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $emailId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $emailContent;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $prospectId;

    /**
     * @MongoDB\Date
     */
    protected $date;


    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getProspectId()
    {
        return $this->prospectId;
    }

    /**
     * @param mixed $prospectId
     */
    public function setProspectId($prospectId)
    {
        $this->prospectId = $prospectId;
    }

    /**
     * @return mixed
     */
    public function getEmailId()
    {
        return $this->emailId;
    }

    /**
     * @param mixed $emailId
     */
    public function setEmailId($emailId)
    {
        $this->emailId = $emailId;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getEmailContent()
    {
        return $this->emailContent;
    }

    /**
     * @param mixed $emailContent
     */
    public function setEmailContent($emailContent)
    {
        $this->emailContent = $emailContent;
    }


}
