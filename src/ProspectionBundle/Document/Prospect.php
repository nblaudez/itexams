<?php
namespace ProspectionBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Prospect
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $societe;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $adresse;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $cp;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $ville;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $etat;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $telephone;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $mobile;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $fax;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $phone1;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $phone2;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $email;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $activite;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $siteweb;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $country;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $quality;

    /**
     * @MongoDB\Collection
     */
    protected $tags;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $needToRecontact = "no";


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $contacte = "no";

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $history;

    /**
     * @param mixed $id
     * @return Prospect
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }

    /**
     * @return mixed
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * @param mixed $cp
     */
    public function setCp($cp)
    {
        $this->cp = $cp;
    }

    /**
     * @return mixed
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * @param mixed $ville
     */
    public function setVille($ville)
    {
        $this->ville = $ville;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * @param mixed $telephone
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;
    }

    /**
     * @return mixed
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param mixed $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param mixed $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return mixed
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * @param mixed $phone1
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;
    }

    /**
     * @return mixed
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @param mixed $phone2
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getActivite()
    {
        return $this->activite;
    }

    /**
     * @param mixed $activite
     */
    public function setActivite($activite)
    {
        $this->activite = $activite;
    }

    /**
     * @return mixed
     */
    public function getSiteweb()
    {
        return $this->siteweb;
    }

    /**
     * @param mixed $siteweb
     */
    public function setSiteweb($siteweb)
    {
        $this->siteweb = $siteweb;
    }

    /**
     * @return mixed
     */
    public function getSociete()
    {
        return $this->societe;
    }

    /**
     * @param mixed $societe
     */
    public function setSociete($societe)
    {
        $this->societe = $societe;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getContacte()
    {
        return $this->contacte;
    }

    /**
     * @param mixed $contacte
     */
    public function setContacte($contacte)
    {
        $this->contacte = $contacte;
    }

    /**
     * @return mixed
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @param mixed $quality
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }

    /**
     * @return mixed
     */
    public function getHistory()
    {
        return $this->history;
    }

    /**
     * @param mixed $history
     */
    public function setHistory($history)
    {
        $this->history = $history;
    }

    /**
     * @return mixed
     */
    public function getNeedToRecontact()
    {
        return $this->needToRecontact;
    }

    /**
     * @param mixed $needToRecontact
     */
    public function setNeedToRecontact($needToRecontact)
    {
        $this->needToRecontact = $needToRecontact;
    }
}
