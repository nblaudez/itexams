<?php
namespace ProspectionBundle\Command;

use ProspectionBundle\Document\Prospect;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('prospection:import')
            ->setDefinition(array(
                new InputOption('file', '', InputOption::VALUE_REQUIRED, 'Filename where are stocked emails'),
                new InputOption('tags', '', InputOption::VALUE_REQUIRED, 'List of tags separated by a coma'),
                new InputOption('country', '', InputOption::VALUE_REQUIRED, 'Name of country'),
            ));
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getOption('file');
        $tags = $input->getOption('tags');
        $country = $input->getOption('country');

        $tags=explode(",",$tags);

        $doctrine = $this->getContainer()->get('doctrine_mongodb');

        if (is_file($file)) {
            $data = file($file);
            foreach($data as $line) {
                $linecsv = str_getcsv(trim($line,'"'),";");
                $prospect = new Prospect();
                $prospect->setSociete(trim($linecsv[0],'"'));
                $prospect->setAdresse($linecsv[1]);
                $prospect->SetCp($linecsv[2]);
                $prospect->setVille($linecsv[3]);
                $prospect->setEtat($linecsv[4]);
                $prospect->setTelephone($linecsv[5]);
                $prospect->setMobile($linecsv[6]);
                $prospect->setFax($linecsv[7]);
                $prospect->setPhone1($linecsv[8]);
                $prospect->setPhone2($linecsv[9]);
                $prospect->setEmail($linecsv[10]);
                $prospect->setDescription($linecsv[11]);
                $prospect->setActivite($linecsv[12]);
                $prospect->setSiteWeb($linecsv[13]);
                $prospect->setTags($tags);
                $prospect->setCountry($country);
                $prospect->setContacte("no");
                $prospect->setQuality(0);

                $em = $doctrine->getManager();
                $em->persist($prospect);
                $em->flush();
            }
        } else {
            exit("Prospects file doesn't exist");
        }
    }
}