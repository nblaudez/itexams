<?php
namespace ProspectionBundle\Command;

use ProspectionBundle\Document\Prospect;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SendCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('prospection:send')
            ->setDefinition(array(
                new InputOption('tags', '', InputOption::VALUE_REQUIRED, 'List of tags separated by a coma'),
                new InputOption('emailId', '', InputOption::VALUE_REQUIRED, 'Email id in db'),
                new InputOption('debug', '', InputOption::VALUE_OPTIONAL, 'debug mode'),
            ));
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tags = $input->getOption('tags');
        $emailId = $input->getOption('emailId');
        $tags=explode(",",$tags);

        $doctrine = $this->getContainer()->get('doctrine_mongodb')->getManager();

        $prospects=$doctrine->getRepository("ProspectionBundle:Prospect")->findBy(["tags"=>['$in'=>$tags]]);
        $output->writeln(count($prospects) ." emails trouvés");

        $email=$doctrine->getRepository("ProspectionBundle:Email")->find($emailId);

        $mailer=$this->getContainer()->get('mailer');



        switch($email->getType()) {
            case "html":$type="text/html";break;
            case "text":$type="text/plain";break;

        }

        if($input->getOption("debug") == true) {
            $message = \Swift_Message::newInstance()
                ->setSubject($email->getTopic())
                ->setFrom('noreply@it-exams.tech')
                ->setTo("blaudez@free.fr")
                ->setBody(
                    $email->getcontent(),
                    $type
                );

            $mailer->send($message);
        } else {
            $progress = new ProgressBar($output, count($prospects));
            $progress->start();
            foreach ($prospects as $prospect) {
                if (filter_var($prospect->getEmail(), FILTER_VALIDATE_EMAIL)) {
                    $message = \Swift_Message::newInstance()
                        ->setSubject($email->getTopic())
                        ->setFrom('noreply@it-exams.tech')
                        ->setTo($prospect->getEmail())
                        ->setBody(
                            str_replace("#EMAIL#",$prospect->getEmail(),$email->getcontent()),
                            $type
                        );

                    sleep(10);
                    $mailer->send($message);
                    $spool = $mailer->getTransport()->getSpool();
                    $transport = $this->getContainer()->get('swiftmailer.transport.real');

                    $spool->flushQueue($transport);

                } else {
                    $doctrine->remove($prospect);
                    $doctrine->flush($prospect);
                }
                $progress->advance();
            }
            $progress->finish();
        }
    }
}