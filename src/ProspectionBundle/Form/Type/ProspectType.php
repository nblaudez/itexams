<?php

namespace ProspectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProspectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('id',HiddenType::class);
        $builder->add('quality',NumberType::class,['label'=>'Quality of prospect']);
        $builder->add('contacte',ChoiceType::class, array('label'=>'Already contacted','choices' => array('no' => 'No', 'yes' => 'Yes')));
        $builder->add('needtorecontact',ChoiceType::class, array('label'=>'Need to recontact','choices' => array('no' => 'No', 'yes' => 'Yes')));
        $builder->add('societe',TextType::class,['label'=>'Company name']);
        $builder->add('adresse',TextType::class,['label'=>'Address']);
        $builder->add('cp',TextType::class,['label'=>'Zip code']);
        $builder->add('ville',TextType::class,['label'=>'City']);
        $builder->add('etat',TextType::class,['label'=>'Country']);
        $builder->add('telephone',TextType::class,['label'=>'Phone number']);
        $builder->add('email',TextType::class,['label'=>'Email']);
        $builder->add('description',TextareaType::class);
        $builder->add('history',TextareaType::class,['attr'=>['rows'=>10]]);
        $builder->add('activite',TextType::class,['label'=>'Activity']);
        $builder->add('siteWeb',TextType::class,['label'=>'Web site']);
    }

    public function getName()
    {
        return 'Prospect';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'ProspectionBundle\Document\Prospect',
        );
    }

}