<?php

namespace ProspectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class VarType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('var');
        $builder->add('value',TextareaType::class,["attr"=>["rows"=>30,"wrap"=>"hard"]]);

    }

    public function getName()
    {
        return 'Var';
    }

    public function getDefaultOptions(array $options)
    {
      
    }
}