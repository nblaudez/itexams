<?php

namespace ProspectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class EmailType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('topic');
        $builder->add('content',TextareaType::class,["attr"=>["rows"=>30,"wrap"=>"hard"]]);
        $builder->add('sender');
        $builder->add('type', ChoiceType::class, ['choices'=>[
            'HTML' => "html",
            'Text' => "text",
        ],
            'choices_as_values' => true,
        ]);
    }

    public function getName()
    {
        return 'Email';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'ProspectionBundle\Document\Email',
        );
    }
}