<?php

namespace ProspectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class TagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('tag');

    }

    public function getName()
    {
        return 'Tag';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
           
        );
    }
}