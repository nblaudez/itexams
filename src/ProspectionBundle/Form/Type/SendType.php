<?php

namespace ProspectionBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SendType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $emails["null"]="choose an email";
        foreach($options["emails"] as $email) {
            $emails[$email->getId()]=$email->getTopic();
        }
        $prospects["null"]="choose a prospect";
        foreach($options["prospects"] as $prospect) {
            $prospects[$prospect->getId()]=$prospect->getEmail();
        }
        $builder->add('prospectId', ChoiceType::class, ["label"=>"Prospect","choices"=>$prospects]);
        $builder->add('emailId', ChoiceType::class, ["label"=>"Email","choices"=>$emails]);
        $builder->add('emailContent', TextareaType::class,["attr"=>["rows"=>30,"wrap"=>"hard"]]);
    }

    public function getName()
    {
        return 'Send';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'ProspectionBundle\Document\EmailSended',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'prospects' => null,
            'emails' => null,
        ));
    }
}