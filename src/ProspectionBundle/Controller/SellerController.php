<?php

namespace ProspectionBundle\Controller;


use CoreBundle\Form\Type\TransactionType;
use CoreBundle\Form\Type\UserDetailsType;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use MongoDBODMProxies\__CG__\CoreBundle\Document\Credit;
use ProspectionBundle\Document\Prospect;
use ProspectionBundle\Document\ProspectSeller;
use ProspectionBundle\Form\Type\ProspectionType;
use ProspectionBundle\Form\Type\ProspectType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


class SellerController extends Controller
{
    public function dashboardAction()
    {
        return $this->render('ProspectionBundle:Vendor:dashboard.html.twig', []);
    }

    public function createAccountAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $user->setVendorId($this->getUser()->getId());
            $userManager->updateUser($user);

            $url = $this->generateUrl('prospection_customers_details', ['userId' => $user->getId()]);
            $response = new RedirectResponse($url);


            //$dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));

            return $response;
        }

        return $this->render('ProspectionBundle:Vendor:createAccount.html.twig', ['form' => $form->createView()]);
    }


    public function prospectAddAction(Request $request) {
        $prospect = new Prospect();

        $form = $this->createForm(new ProspectType(), $prospect);

        if ($request->isMethod('post')) {

            $prospect = $form->handleRequest($request)->getData();


            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->persist($prospect);
            $dm->flush();

            $this->addFlash("success", "prospects has been updated");

            return $this->redirectToRoute('prospection_prospects');
        }
        return $this->render('ProspectionBundle:Vendor:prospectAdd.html.twig', ["form" => $form->createView()]);
    }

    /**
     * @param int $page
     * @return Response
     */
    public function prospectAction(Request $request)
    {

        $session = new Session();
        if($session->get('tag') == null) {
            $session->set('tag',"recrutement");
            $session->set('country',"fr");
        }
        $form = $this->createForm(new ProspectType());

        if ($request->isMethod('post')) {

            $data = $form->handleRequest($request)->getData();
            $prospect =  $this->get('doctrine_mongodb')->getRepository('ProspectionBundle:Prospect')->find($data['id']);
            $dm = $this->get('doctrine_mongodb')->getManager();
            if($request->get('form_action')=="delete") {
                $dm->remove($prospect);
                $dm->flush();
                $this->addFlash("success", "prospects has been deleted");
            } elseif($request->get('form_action')=='Search') {

                $session->set('tag',$request->get('tag'));
                $session->set('country',$request->get('country'));

            } else {
                $prospect->setQuality($data['quality']);
                $prospect->setContacte($data['contacte']);
                $prospect->setSociete($data['societe']);
                $prospect->setAdresse($data['adresse']);
                $prospect->setCp($data['cp']);
                $prospect->setVille($data['ville']);
                $prospect->setEtat($data['etat']);
                $prospect->setEmail($data['email']);
                $prospect->setTelephone($data['telephone']);
                $prospect->setNeedToRecontact($data['needtorecontact']);
                $prospect->setDescription($data['description']);


                $dm->persist($prospect);
                $dm->flush();
                $this->addFlash("success", "prospects has been updated");
            }
        }
        return $this->render('ProspectionBundle:Vendor:prospect.html.twig', ["form" => $form->createView()]);
    }

    public function prospectDetailsAction($id) {
        $prospect = $this->get('doctrine_mongodb')->getRepository('ProspectionBundle:Prospect')->find($id);
        $serializer = $this->get('serializer');
        $json = $serializer->serialize($prospect, 'json');
        return new Response($json, 200, ['Content-Type' => 'application/json']);
    }

    public function prospectAjaxAction(Request $request)
    {
        $qb = $this->get('doctrine_mongodb')->getManager()->createQueryBuilder('ProspectionBundle:Prospect');

        $qb->sort('quality', 'DESC')
            ->sort('contacte', 'ASC')
            ->sort('societe', 'ASC');

        $prospects = $qb->getQuery()->execute();


        $count = $qb->getQuery()->execute()->count();

        $output = array(
            'data' => array(),
            'recordsFiltered' => $count,
            'recordsTotal' => $count
        );


        foreach($prospects as $prospect) {
            $output['data'][] = [$prospect->getSociete(), $prospect->getTelephone(), $prospect->getVille(), $prospect->getContacte(),$prospect->getNeedtoReContact(),'<button data-id-prospect="'.$prospect->getId().'" type="button" class="btn btn-primary" data-toggle="modal" data-target="#detailsModal">Details</button>'];
        }


        return new Response(json_encode($output), 200, ['Content-Type' => 'application/json']);
    }


    public function customerAction()
    {

        $users = $this->get('doctrine_mongodb')->getRepository("CoreBundle:User")->findByVendorId($this->getUser()->getId());
        return $this->render('ProspectionBundle:Vendor:customer.html.twig', ["users" => $users]);
    }

    public function customerDetailsAction(Request $request, $userId)
    {
        $user = $this->get('doctrine_mongodb')->getRepository("CoreBundle:User")->find($userId);
        $transactions = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Order")->findByUserId($userId);
        $form = $this->createForm(UserDetailsType::class, $user);
        $credits = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Credit")->findByUserId($userId);
        if ($credits) {
            $credits = $credits[0];
        } else {
            $credits = new Credit();
            $credits->setUserId($userId);
        }

        if ($request->isMethod("post")) {
            if ($request->get('form_action') == "Update credits") {


                $credits->setCredits($request->get('credits'));

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($credits);
                $dm->flush();
                $this->addFlash("success", "credits has been updated");
            } else {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $form->getData();

                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($user);

                    $this->addFlash("success", "User has been updated");

                }
            }
        }


        return $this->render('ProspectionBundle:Vendor:customerDetails.html.twig', ["credits" => $credits, "user" => $user, "form" => $form->createView(), "transactions" => $transactions]);

    }

}