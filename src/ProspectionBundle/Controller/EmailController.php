<?php

namespace ProspectionBundle\Controller;


use CoreBundle\Form\Type\TransactionType;
use ProspectionBundle\Document\EmailSended;
use ProspectionBundle\Document\Email;
use ProspectionBundle\Document\Prospect;
use ProspectionBundle\Form\Type\EmailType;
use ProspectionBundle\Form\Type\ProspectionType;
use ProspectionBundle\Form\Type\ProspectType;
use ProspectionBundle\Form\Type\SendType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class EmailController extends Controller
{
    public function indexAction()
    {
        return $this->render('ProspectionBundle:Email:index.html.twig');
    }

    public function emailAction(Request $request) {
        
        $email=new Email();
        $em = $this->get("doctrine_mongodb")->getManager();

        if($request->get('action')=="delete")  {
            $email = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Email")->find($request->get('emailId'));
            $em->remove($email);
            $em->flush();
            $this->addFlash("success", "Email has been deleted");
        }
        if($request->isMethod("post")) {
            $form=$this->createForm(EmailType::class,$email);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $email = $form->getData();

                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($email);
                $em->flush();$this->addFlash("success", "Email has been added");
            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        $emails = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Email")->findAll();
        return $this->render('ProspectionBundle:Email:email.html.twig',['emails'=>$emails]);
    }

    public function sendAction(Request $request) {

        $prospects = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->findAll();
        $emails = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Email")->findAll();
        $emailSended = new EmailSended();

        $form= $this->createForm(SendType::class,$emailSended,["prospects"=>$prospects,"emails"=>$emails]);


        if($request->isMethod("post")) {
            $form->handleRequest($request);
            if ($form->isValid()) {

                $emailSended = $form->getData();
                $prospect = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->find($emailSended->getProspectId());
                $email = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:email")->find($emailSended->getEmailId());

                // Create a message
                $message = \Swift_Message::newInstance($email->getTopic())
                    ->setFrom($email->getSender())
                    ->setTo($prospect->getEmail())
                    ->setBody($emailSended->getEmailContent(),'text/html');

                $this->get('mailer')->send($message);




                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($emailSended);
                $em->flush();
                $this->addFlash("success", "Email has been added and sent");

            }
        }

        return $this->render('ProspectionBundle:Email:send.html.twig',["emails"=>$emails,"prospects"=>$prospects,'form'=>$form->createView()]);
    }

    public function prospectsAction(Request $request) {
        $em = $this->get("doctrine_mongodb")->getManager();
        if($request->get('action')=="delete")  {
            $prospect = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->find($request->get('prospectId'));
            $em->remove($prospect);
            $em->flush();
            $this->addFlash("success", "Prospect has been deleted");
        }
        if($request->isMethod("post")) {
            $prospect = new Prospect();
            $form=$this->createForm(ProspectType::class,$prospect);
            $form->handleRequest($request);
            if ($form->isValid()) {
                $prospect = $form->getData();

                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($prospect);
                $em->flush();$this->addFlash("success", "Prospect has been added");
            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }
        $prospects = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->findAll();
        return $this->render('ProspectionBundle:Email:prospects.html.twig',["prospects"=>$prospects]);
    }

    public function getMailContentAction($emailId) {
        $email = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Email")->find($emailId);
        return new JsonResponse(["content"=>$email->getContent()]);
    }


    public function addEmailAction() {
        $form= $this->createForm(EmailType::class);
        return $this->render('ProspectionBundle:Email:addEmail.html.twig',['form'=>$form->createView()]);
    }

    public function updateEmailAction(Request $request,$emailId) {

        $email = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Email")->find($request->get('emailId'));
        $form=$this->createForm(EmailType::class,$email);

        if($request->isMethod("post")) {

            $form->handleRequest($request);
            if ($form->isValid()) {
                $email = $form->getData();

                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($email);
                $em->flush();
                $this->addFlash("success", "Email has been updated");
            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('ProspectionBundle:Email:updateEmail.html.twig',['form'=>$form->createView(),"emailId"=>$emailId]);
    }

    public function addProspectAction() {
        $prospect = new Prospect();
        $form=$this->createForm(ProspectType::class,$prospect);
        return $this->render('ProspectionBundle:Email:addProspect.html.twig',['form'=>$form->createView()]);
    }

    public function updateProspectAction(Request $request,$prospectId) {
        $prospect = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->find($request->get('prospectId'));
        $form=$this->createForm(ProspectType::class,$prospect);

        if($request->isMethod("post")) {

            $form->handleRequest($request);
            if ($form->isValid()) {
                $prospect = $form->getData();

                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($prospect);
                $em->flush();
                $this->addFlash("success", "Prospect has been updated");
            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        return $this->render('ProspectionBundle:Email:updateProspect.html.twig',['form'=>$form->createView(),'prospectId'=>$prospectId]);
    }

    public function prospectHistoryAction() {
        return $this->render('ProspectionBundle:Email:historyProspect.html.twig',[]);
    }
}