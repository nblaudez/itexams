<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GuestExerciceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tests = [];
        foreach ($options["exercices"] as $exercice) {
            $exercices[$exercice->getId()] = $exercice->getName();
        }

        $builder->add('firstname');
        $builder->add('lastname');
        $builder->add('email', TextType::class);
        $builder->add('exercice', ChoiceType::class, ["attr"=>["class"=>"input-lg"],"choices"=>$exercices,"attr"=>["class"=>"select2"]]);
        $builder->add('language', ChoiceType::class, ["attr"=>["class"=>"input-lg"],"choices"=>["html","c","java","python","php","javascript","ruby"],"attr"=>["class"=>"select2"]]);
    }

    public function getName()
    {
        return 'guestExercice';
    }



    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'exercices' => null
        ));
    }
}