<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class GuestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tests = [];
        foreach ($options["tests"] as $test) {
            $tests[$test->getId()] = $test->getName();
        }

        $builder->add('firstname');
        $builder->add('lastname');
        $builder->add('email', TextType::class);
        $builder->add('test', ChoiceType::class, ["choices"=>$tests,"attr"=>["class"=>"select2"]]);
    }

    public function getName()
    {
        return 'guest';
    }



    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'tests' => null
        ));
    }
}