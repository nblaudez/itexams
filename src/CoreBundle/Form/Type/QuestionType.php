<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\Type\QuestionCategory;
use Symfony\Component\OptionsResolver\OptionsResolver;


class QuestionType extends AbstractType
{
    protected $_db;

    protected $_categories;



    public function buildForm(FormBuilderInterface $builder, array $options)
    {



        $builder->add('question', TextareaType::class,["label"=>"Question content", "attr"=>["rows"=>10,"wrap"=>"hard"]]);
        $builder->add('tags', TextType::class, ['data'=>$options['tags'], 'attr' => ['data-role' => 'tagsinput']]);
        $builder->add('categoryId', ChoiceType::class, ["label"=>"Category","choices"=>$options["categories"]]);
        $builder->add('explanation', TextareaType::class);
        $builder->add('answers', CollectionType::class, [
            "label"=>"List of answers",
            'entry_type' => ResponseType::class,
            'allow_add' => true,
            'allow_delete'  => true,
            'entry_options' => [
                'required' => true,
            ]
        ]);
    }

    public function getName()
    {
        return 'question';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CoreBundle\Document\Question',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'categories' => null,
            'tags'=> null
        ));
    }
}