<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('numberOfQuestions');
        $builder->add('activated', ChoiceType::class, ['choices'=>[
            'Yes' => true,
            'No' => false,
        ],
            'choices_as_values' => true,
        ]);
    }

    public function getName()
    {
        return 'questioncategory';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CoreBundle\Document\QuestionCategory',
        );
    }
}