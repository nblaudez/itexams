<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\Type\QuestionCategory;


class ResponseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('response', TextareaType::class);
        $builder->add('goodResponse', ChoiceType::class, ['choices'=>[
            "It's a good response" => true,
            "It's not a good response" => false,
        ],
            'choices_as_values' => true,
        ]);
    }

    public function getName()
    {
        return 'response';
    }
}