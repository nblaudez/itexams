<?php

namespace CoreBundle\Form\Type;

use Doctrine\Bundle\MongoDBBundle\Form\Type\DocumentType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\Type\QuestionCategory;


class TestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('technologyId', DocumentType::class, array('class' => 'CoreBundle:Sector','choice_value'=>'id','choice_label' => 'name'));

        $builder->add('name');
        $builder->add('description', TextareaType::class);
        $builder->add('instructions', TextareaType::class);
        //$builder->add('tags',TextType::class,array('attr' => array('data-role'=>'tagsinput')));
        $builder->add('time');
        $builder->add('activated', ChoiceType::class, ['choices'=>[
        'Yes' => true,
        'No' => false,
        ],
                'choices_as_values' => true,
        ]);
    }

    public function getName()
    {
        return 'test';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CoreBundle\Document\Test',
        );
    }
}