<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\Type\QuestionCategory;
use Symfony\Component\OptionsResolver\OptionsResolver;


class QuestionExerciceType extends AbstractType
{
    protected $_db;

    protected $_categories;



    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('question', TextareaType::class,["label"=>"Question content", "attr"=>["rows"=>50,"wrap"=>"hard"]]);
    }

    public function getName()
    {
        return 'questionexercice';
    }



    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(

        ));
    }
}