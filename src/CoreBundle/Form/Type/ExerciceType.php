<?php

namespace CoreBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Form\Type\QuestionCategory;


class ExerciceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('description', TextareaType::class);
        $builder->add('instructions', TextareaType::class);
        $builder->add('content', TextareaType::class);
        $builder->add('tags',TextType::class,array('attr' => array('data-role'=>'tagsinput')));
        $builder->add('numberQuestions');
        $builder->add('time');
        $builder->add('activated', ChoiceType::class, ['choices'=>[
        'Yes' => true,
        'No' => false,
        ],
                'choices_as_values' => true,
        ]);
        $builder->add('questions', CollectionType::class, [
            "label"=>"List of questions",
            'entry_type' => QuestionExerciceType::class,
            'allow_add' => true,
            'entry_options' => [
                'required' => true,
            ]
        ]);
    }

    public function getName()
    {
        return 'exercice';
    }

    public function getDefaultOptions(array $options)
    {
        return array(
            'data_class' => 'CoreBundle\Document\Exercice',
        );
    }
}