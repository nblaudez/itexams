<?php

namespace CoreBundle\Twig;

class ITExamsExtension extends \Twig_Extension
{

    public function __construct($doctrine) {
        $this->_doctrine = $doctrine;

    }
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('sector', array($this, 'sector')),
            new \Twig_SimpleFilter('category', array($this, 'category')),
            new \Twig_SimpleFilter('gmdate', array($this, 'gmdate')),
        );
    }

    public function getFunctions() {
        return [new \Twig_SimpleFunction('fileExists',array($this,'fileExists'))];
    }

    public function fileExists($file) {
        return file_exists(__DIR__."/".$file);
    }

    public function sector($id)
    {
        $sector = $this->_doctrine->getRepository("CoreBundle:Sector")->find($id);
        return $sector->getName();
    }

    public function category($id,$testId)
    {

        $test = $this->_doctrine->getRepository("CoreBundle:Test")->find($testId);
        foreach($test->getCategories() as $cat) {
            if($cat->getId()==$id) {
                return $cat->getName();
            }
        }
    }

    public function gmdate($string) {

        return gmdate("H:i:s",$string);
    }

    public function getName()
    {
        return 'testmyskills_extension';
    }


}