<?php
namespace CoreBundle\EventListener;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Listener responsible to change the redirection at the end of the password resetting
 */
class UserRegister implements EventSubscriberInterface
{

    private $mongodb;

    public function __construct($mongodb, $container) {
        $this->mongodb = $mongodb;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_COMPLETED => 'setAffiliateAccount',
        );
    }

    public function setAffiliateAccount(FilterUserResponseEvent $event)
    {
        if($this->container->get('request')->cookies->get('AFFILIATE_ID') != null) {
            $user=$this->mongodb->getRepository("CoreBundle:User")->find($event->getUser()->getId());
            $user->setAffiliateId($this->container->get('request')->cookies->get('AFFILIATE_ID'));

            $em = $this->mongodb->getManager();
            $em->persist($user);
            $em->flush();
        }
        return true;

    }
}