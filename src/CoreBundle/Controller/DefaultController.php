<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\TakenTest;
use CoreBundle\Document\TakenTestQuestion;
use CoreBundle\Document\Unsuscribe;
use CoreBundle\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CoreBundle\Form\Type\GuestType;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {

        $response = $this->render('CoreBundle:Default:index.html.twig');

        if ($request->get("AFFILIATE_ID") != null) {
            $response->headers->setCookie(new Cookie('AFFILIATE_ID', $request->get("AFFILIATE_ID")));
        }
        return $response;
    }

    public function exercicesAction()
    {
        $exercices = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findAll();
        return $this->render('CoreBundle:Default:exercices.html.twig', array("exercices" => $exercices));
    }


    public function exerciceDetailsAction($slug)
    {

        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findByNameSlugued($slug)[0];
        return $this->render('CoreBundle:Default:exerciceDetails.html.twig', array('exercice' => $exercice));
    }

    public function sectorsAction()
    {
        $testsResult = [];
        $sections = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Section")->findBy([], ["order" => "ASC"]);
        foreach ($sections as $section) {
            $sectors = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findBy(["sectionId" => $section->getId()]);
            foreach ($sectors as $sector) {
                $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findBy(["sectorId" => $sector->getId(), "userId" => null, "activated" => true]);
                $number_tests[$sector->getSlug()] = count($tests);

                $testsResult[$section->getId()][$sector->getCategory()][] = $sector;
            }
        }

        ksort($testsResult);

        return $this->render('CoreBundle:Default:sectors.html.twig', array(
            'sections' => $sections,
            'tests' => $testsResult,
            'number_tests' => $number_tests,
        ));
    }


    public function aboutusAction()
    {
        return $this->render('CoreBundle:Default:aboutus.html.twig', array());
    }

    public function testsAction($technologySlug)
    {

        $technology = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findBySlug($technologySlug)[0];

        $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findBy(['technologyId' => $technology->getId(), 'userId' => null, "activated" => true]);
        return $this->render('CoreBundle:Default:tests.html.twig', array(
            'tests' => $tests,
            'technology' => $technology,
        ));
    }

    public function testdetailsAction(Request $request, $testSlug)
    {
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findOneBynameSlugued($testSlug);

        return $this->render('CoreBundle:Default:testdetails.html.twig', array(
            "test" => $test
        ));
    }


    public function faqAction()
    {
        return $this->render('CoreBundle:Default:faq.html.twig', array());
    }


    public function contactAction(Request $request)
    {

        $formErrors = [];
        $sended = false;

        if ($request->isMethod("post")) {
            if ($request->get("name") == "") {
                $formErrors[] = "you must enter a name";
            } else {
                $name = $request->get("name");
            }
            if (filter_var($request->get("email"), FILTER_VALIDATE_EMAIL) === false) {
                $formErrors[] = "you must enter valid email address";
            } else {
                $email = $request->get("email");
            }
            if ($request->get("subject") == "") {
                $formErrors[] = "you must enter a subject";
            } else {
                $subject = $request->get("subject");
            }
            if ($request->get("message") == "") {
                $formErrors[] = "you must enter a message";
            } else {
                $message = $request->get("message");
            }

            if (count($formErrors) == 0) {
                $messageBody = <<<EOF
You received a message from the site :<br />
<br />
name : $name<br />
email : $email<br />
subject: $subject<br />
message :<br /><br />
$message<br />
EOF;

                $message = \Swift_Message::newInstance()
                    ->setSubject("you received a message from IT-EXAMS")
                    ->setFrom($email)
                    ->setTo("sales@it-exams.tech")
                    ->setBody(
                        $messageBody,
                        'text/html'
                    );
                if ($this->get("mailer")->send($message)) {
                    $sended = true;
                }
            }
        }
        return $this->render('CoreBundle:Default:contact.html.twig', array("sended" => $sended, "formErrors" => $formErrors));
    }


    public function pricesAction()
    {
        return $this->render('CoreBundle:Default:prices.html.twig', []);
    }

    public function featuresAction()
    {
        $sectors = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findAll();
        return $this->render('CoreBundle:Default:features.html.twig', ['sectors' => $sectors]);
    }

    public function termsofuseAction()
    {
        return $this->render('CoreBundle:Default:termsofuse.html.twig', []);
    }

    public function unsubscribeAction($email)
    {

        $exists = $this->get("doctrine_mongodb")->getRepository("ProspectionBundle:Prospect")->findByEmail($email);
        if (count($exists) == 0) {
            $result = "2";
        } else {
            $em = $this->get("doctrine_mongodb")->getManager();
            $em->remove($exists[0]);
            $em->flush();
            $result = "1";
        }
        return $this->render('CoreBundle:Default:unsuscribe.html.twig', ['result' => $result]);
    }

    public function sluggify($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
        $slug = str_replace(array("\r", "\n"), '', $slug);
        return $slug;
    }
}
