<?php
namespace CoreBundle\Controller;

use CoreBundle\Document\Credit;
use CoreBundle\Document\Invoice;
use CoreBundle\Document\Order;
use CoreBundle\Form\Type\UserInfosType;
use CoreBundle\Libs\PayPal;
use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Tests\RequestContentProxy;
use \Symfony\Component\HttpFoundation\Response;

class PaymentController extends Controller
{
    public function prepareAction($plan)
    {
        if($this->getUser() == null) {
            return $this->redirectToRoute('fos_user_registration_register');
        }
        $user =  $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($this->getUser()->getId());

        if (!$user->hasUserInfos()) {
            return $this->redirect($this->generateUrl("testmyskills_payment_userinfos",['plan'=>$plan]));
        } else {

            switch ($plan) {
                case "basic":
                    $itemName = "Plan basic";
                    $itemPrice = "Free";
                    $itemDesc = "Plan Basic";
                    $nbCredit="10";
                    break;
                case "professional":
                    $itemName = "Plan enterprise";
                    $itemPrice = "500";
                    $itemDesc = "Plan professional";
                    $nbCredit="500";
                    break;
                case "enterprise":
                    $itemName = "Plan enterprise";
                    $itemPrice = "150";
                    $itemDesc = "IT-Exams Plan enterprise";
                    $nbCredit="150";
                    break;
            }

            $order = new Order();
            $order->setDate(time());
            $order->setUserId($this->getUser()->getId());
            $order->setStatus(0);
            $order->setAmount($itemPrice);
            $order->setOfferName($itemName);

            $em = $this->get("doctrine_mongodb")->getManager();


            $em->persist($order);
            $em->flush($order);

            $url = "https://paiement-securise.bluepaid.com/in.php?set_secure_return=true&set_secure_conf=true&id_boutique=21421www&email_client=" . $this->getUser()->getEmail() . "&id_client=" . $order->getId() . "&montant=$itemPrice&devise=EUR&langue=GB";

            return $this->redirect($url);
        }
    }

    public function userinfosAction(Request $request, $plan) {

        $user = $this->getUser();
        $form =  $this->createForm(UserInfosType::class, $user);

        $hasAPlan=$this->get("itexams.user")->hasAPlan($user->getId());

        if($request->isMethod("POST")) {
            $form->handleRequest($request);
            $user = $form->getData();
            $em = $this->get("doctrine_mongodb")->getManager();
            $em->persist($user);
            $em->flush($user);

            return $this->render('CoreBundle:Payment:userinfos.html.twig', ['form' => $form->createView(),'saved' => true,'hasAPlan'=>$hasAPlan]);

        } else {
            return $this->render('CoreBundle:Payment:userinfos.html.twig', ['form' => $form->createView(),'hasAPlan'=>$hasAPlan]);
        }
    }

    public function endAction(Request $request) {

        $order =  $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->find($request->get('id_client'));
        return $this->render('CoreBundle:Payment:end.html.twig', ["order"=>$order,"status"=>$request->get('etat')]);
    }

    public function cancelACtion() {
        return $this->render('CoreBundle:Payment:cancel.html.twig', []);
    }

    public function confirmationAction(Request $request) {

        $em = $this->get("doctrine_mongodb")->getManager();

        $bluePaidIps=["119.63.142.64","193.33.47.34","193.33.47.35","193.33.47.39","87.98.218.80","213.245.150.17"];
        if(in_array($_SERVER["REMOTE_ADDR"],$bluePaidIps)) {
            if($request->isMethod("POST")) {
                $orderId=$request->get('id_client');
                $status=$request->get('etat');

                $order = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->find($orderId);
                $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($order->getUserId());
                $config = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Config")->findByName("nextInvoiceId")[0];



                if($status=="ok") {
                    $order->setStatus(1);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Your order from IT-Exams.tech has been refused')
                        ->setFrom('contact@it-exams.tech')
                        ->setTo($user->getEmail())
                        ->setContentType("text/html")
                        ->setBody(
                            $this->renderView(
                                'Emails/orderRefused.html.twig',
                                array(
                                    'order' => $order,
                                    'user' => $user
                                )
                            ),
                            'text/html'
                        );


                    switch ($order->getOfferName()) {
                        case "Plan basic":
                            $nbCredit="10";
                            break;
                        case "Plan enterprise":
                            $nbCredit="100";
                            break;
                        case "Plan enterprise":
                            $nbCredit="500";
                            break;
                    }

                    $invoice = new Invoice();
                    $invoice->setDate(time());
                    $invoice->setOrderId($order->getId());
                    $invoice->setInvoiceId($config->getValue());
                    $em->persist($invoice);



                    $credit=new Credit();
                    $credit->setUserId($user->getId());
                    $credit->setCredits($nbCredit);
                    $em->persist($credit);


                    $config->setValue($config->getValue()+1);
                    $em->persist($config);


                    $em->flush();




                } else {
                    $order->setStatus(-1);

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Your order from IT-Exams.tech has been accepted')
                        ->setFrom('contact@it-exams.tech')
                        ->setTo($user->getEmail())
                        ->setContentType("text/html")
                        ->setBody(
                            $this->renderView(
                                'Emails/orderSuccess.html.twig',
                                array(
                                    'order' => $order,
                                    'user' => $user
                                )
                            ),
                            'text/html'
                        );
                }


                $this->get('mailer')->send($message);

            }


            $em->persist($order);
            $em->flush($order);
        }

        die("ok");
    }

    public function ordersListAction() {

        $orders = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->findBy(["userId"=>$this->getUser()->getId()],["date"=>"desc"]);
        return $this->render('CoreBundle:Payment:orderList.html.twig', ["orders"=>$orders]);

    }

    public function orderDetailsAction($orderId) {
        $order = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->find($orderId);
        return $this->render('CoreBundle:Payment:orderDetails.html.twig', ["order"=>$order]);
    }

    public function invoiceDetailsHTMLAction($orderId) {
        $order = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->find($orderId);
        $invoice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Invoice")->findByOrderId($order->getId())[0];
        $user= $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($this->getUser()->getId());

        return $this->render('CoreBundle:Payment:invoice.html.twig', ["user"=>$user,"order"=>$order,"invoice"=>$invoice]);
    }

    public function invoiceDetailsAction($orderId) {

        $order = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->find($orderId);
        $invoice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Invoice")->findByOrderId($order->getId())[0];
        $user= $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($this->getUser()->getId());

        $html = $this->renderView('CoreBundle:Payment:invoice.html.twig', ["user"=>$user,"order"=>$order,"invoice"=>$invoice]);

        $dompdf = $this->get('slik_dompdf');
        $dompdf->getpdf($html);
        $dompdf->stream("invoice_".$invoice->getInvoiceId().".pdf");
    }
}