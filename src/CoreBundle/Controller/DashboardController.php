<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\Customisation;
use CoreBundle\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class DashboardController extends Controller
{
    public function cleanSession() {
        $session = new Session();
        $session->set("testQuestions", null);
        $session->set("takenTestId", null);
        $session->set("questionId", null);
    }
    public function dashboardAction()
    {
        $this->cleanSession();
        $tests = $this->get("itexams.test")->retrieveUserTests($this->getUser()->getId());
        $exercices = $this->get("itexams.exercice")->retrieveUserExercice($this->getUser()->getId());
        $users = $this->get("itexams.user")->retrieveCompanyUser($this->getUser()->getId());
        $graphDatas = $this->get("itexams.user")->retrieveAllgraphData($this->getUser()->getId());
        //dump($graphDatas);die;
        return $this->render('CoreBundle:Dashboard:dashboard.html.twig', array("graphDatas"=>$graphDatas,"tests" => $tests, "exercices" => $exercices, "users"=>$users));
    }



    public function creditsAction($message=null) {
        $this->cleanSession();
        $credits=$this->get("doctrine_mongodb")->getRepository("CoreBundle:Credit")->findByUserId($this->getUser()->getId());

        if(isset($credits[0])) {
            $credits=$credits[0];
        } else {
            $credits = false;
        }

        return $this->render('CoreBundle:Dashboard:credits.html.twig', array(
            "credits" => $credits,
            "message"=>$message
        ));
    }

    public function affiliationAction(Request $request) {
        $this->cleanSession();
        $saveBankInformations = false;
        $invitationSended = false;
        $saveWithdrawal = false;
        $email = null;
        $errors=[];

        if($request->isMethod("post")) {
            if($request->get("form_action")=="saveBankInformations") {
                $result = $this->get("itexams.affiliation")->setBankInfo($this->getUser()->getId(), $request->get("iban"), $request->get("swift"));
                if ($result !== true && count($result) > 0) {
                    $errors = $result;
                } else {
                    $saveBankInformations = true;
                }
            } elseif($request->get("form_action")=="askWidthdrawal") {
                $this->get("itexams.affiliation")->createWidthdrawal($this->getUser(),$request->get("amount"));
                $saveWithdrawal = true;
            } elseif($request->get("form_action")=="inviteUser") {
                $this->get("itexams.affiliation")->invite($this->getUser(),$request->get("email"));
                $invitationSended = true;
                $email=$request->get("email");
            }
        }

        $currentUser=$this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($this->getUser()->getId());
        $users=$this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->findByAffiliateId($this->getUser()->getId());
        $orders=[];
        $amount = $totalAmount = 0 ;
        $results=[];
        foreach($users as $user) {
            foreach($this->get("doctrine_mongodb")->getRepository("CoreBundle:Order")->findByUserId($user->getId()) as $order) {

                if($order->getStatus()==1) {
                    $amount+=$order->getAmount();
                    $totalAmount+=$amount;
                }
            }
            $results[]=["user"=>$user,"orderAmount"=> $amount*10/100 ];
        }

        $widthdrawals=$this->get("doctrine_mongodb")->getRepository("CoreBundle:Withdrawal")->findByUserId($this->getUser()->getId());

        return $this->render('CoreBundle:Dashboard:affiliation.html.twig', array(
            "users" => $users,
            "invitationSended" => $invitationSended,
            "email" => $email,
            "users" => $results,
            "currentUser" => $currentUser,
            "errors"=>$errors,
            "saveBankInformations" => $saveBankInformations,
            "totalAmount" => $totalAmount*10/100,
            "saveWithdrawal" => $saveWithdrawal,
            "withdrawals"=>$widthdrawals,
            "user"=>$this->getUser(),
        ));
    }

    public function userResultsAction($userId) {
        $this->cleanSession();
        $tests=$this->get("itexams.test")->retrieveAllUserResults($userId);
        foreach($tests as $key=>$test) {
            $tests[$key]["test"] = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->find($test['takenTest']->getTestId());
        }
        $results["tests"]=$tests;

        $exercices = $this->get("itexams.exercice")->retrieveUserExercice($userId);
        $results["exercices"]=$exercices;

        $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($userId);
        
        $graphResults = $this->get("itexams.user")->retrieveGraphData($this->getUser()->getId(),$userId);



        return $this->render('CoreBundle:Dashboard:userResults.html.twig', array(
            'results'=>$results,
            'user'=>$user,
            'graphResults'=>$graphResults
        ));
    }

    public function customisationAction(Request $request) {

        $customisation= $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($this->getUser()->getId());

        if(count($customisation) > 0)
            $customisation=$customisation[0];
        else
            $customisation=new Customisation();

        if($request->isMethod("post")) {
            $customisation->setName($request->get('companyName'));
            $customisation->setUserId($this->getUser()->getId());
            $customisation->setContactEmail($request->get('contactEmail'));
            $em = $this->get("doctrine_mongodb")->getManager();
            $em->persist($customisation);
            $em->flush();

            $this->addFlash("success", "Information has been updated");
        }

        return $this->render('CoreBundle:Dashboard:customisation.html.twig', array("customisation"=>$customisation));

    }

}