<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\Category;
use CoreBundle\Document\Tag;
use CoreBundle\Document\Test;
use CoreBundle\Document\Question;
use CoreBundle\Form\Type\QuestionType;
use CoreBundle\Form\Type\CategoryType;
use CoreBundle\Form\Type\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ManageTestController extends Controller
{
    public function indexAction(Request $request)
    {
        if ($request->query->get("linkAction") == "delete") {
            if ($request->query->get("linkAction") == "delete") {

                $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($request->query->get("testId"));

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->remove($test);
                $dm->flush();

                $this->addFlash("success", "Test has been deleted");

            }

        }

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $tests = $test->findByUserId($this->getUser()->getId());


        return $this->render('CoreBundle:Manage:index.html.twig', ["tests" => $tests]);

    }



    public function updateTestAction(Request $request, $testId)
    {


        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $test = $test->find($testId);

        $form = $this->createForm(TestType::class, $test);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $test = $form->getData();


                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Test has been updated");

                return $this->redirect($this->generateUrl("testmyskills_manage_tests"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:Manage:updateTest.html.twig', ["test" => $test, "form" => $form->createView()]);

    }


    public function addTestAction(Request $request)
    {

        $test = new Test();
        $form = $this->createForm(TestType::class, $test);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                for($i=0;$i<=10;$i++) {
                    $results[]=["value"=>$i*10,"result"=>"0"];
                }
                $test = $form->getData();
                $test->setResults($results);
                $test->setNameSlugued($this->sluggify($test->getName()));
                $test->setCore(0);
                $tags = explode(",", $test->getTags());
                $test->setTags($tags);
                $test->setUserId($this->getUser()->getId());
                $test->setCore(0);
                $tags=$test->getTags();
                $tags=explode(",",$tags);
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                    }
                }

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Test has been created");

                return $this->redirect($this->generateUrl("testmyskills_manage_tests"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        return $this->render('CoreBundle:Manage:addTest.html.twig', array('form' => $form->createView()));
    }


    public function questionsAction(Request $request, $testId)
    {

        $testRepository = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $test = $testRepository->find($testId);

        $questionRepository = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Question");


        if ($request->query->get("linkAction") == "delete") {

            $question = $questionRepository->find($request->query->get("questionId"));
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->remove($question);
            $dm->flush();

            $this->addFlash("success", "Question has been deleted");

        }

        $questions=$questionRepository->findByTestId($testId);
        
        return $this->render('CoreBundle:Manage:testQuestions.html.twig', ["test" => $test,'questions'=>$questions]);

    }

    public function updateQuestionAction(Request $request)
    {

        $testId = $request->query->get("testId");
        $questionId = $request->query->get("questionId");

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);
        $question = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Question")->find($questionId);

        $tagString='';
        $tags=$question->getTags();
        foreach($tags as $tag) {
            $tagString.=$tag->getName().",";
        }
        $tagString=rtrim($tagString,",");
        foreach($test->getCategories() as $category) {
            $categories[$category->getId()] = $category->getName();
        }
        $form = $this->createForm(QuestionType::class, $question, ["tags"=>$tagString,"categories" => $categories]);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $question = $form->getData();
                $question->setTestId($testId);


                $category=$this->get("doctrine_mongodb")->getRepository("CoreBundle:Category")->find($question->getCategoryId());
                $question->setCategoryId($category);


                $tags=$question->getTags();
                $tags=explode(",",$tags);
                $question->cleanTags();
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $question->addTag($mTag);


                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                    }
                }


                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($question);
                $dm->flush();

                $this->addFlash("success", "Question has been updated");

                return $this->redirect($this->generateUrl("testmyskills_manage_questions", ["testId" => $test->getId()]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:Manage:testQuestionUpdate.html.twig', ["form" => $form->createView(), "test" => $test]);

    }

    public function addQuestionAction(Request $request)
    {

        $no_categories = false;
        $testId = $request->query->get('testId');

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);
        $question = new Question();
        foreach($test->getCategories() as $category) {
            $categories[$category->getId()] = $category->getName();
        }
        $form = $this->createForm(QuestionType::class, $question, ["categories" => $categories]);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $question = $form->getData();
                $question->setTestId($testId);

                $category=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Category")->find($question->getCategoryId());
                $question->setCategoryId($category);

                $tags=$question->getTags();
                $tags=explode(",",$tags);
                $cTags=[];
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                        $cTags[]=$mTag;
                    }
                }
                $question->setTags($cTags);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($question);
                $dm->flush();

                $this->addFlash("success", "Question has been created");

                return $this->redirect($this->generateUrl("testmyskills_manage_questions", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        if(count($test->getCategories())==0) {
            $no_categories = true;
        }

        return $this->render('CoreBundle:Manage:testQuestionAdd.html.twig', ["no_categories"=>$no_categories,"test" => $test, "form" => $form->createView()]);
    }


    public function categoriesAction(Request $request, $testId)
    {
        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);

        if ($request->query->get("linkAction") == "delete") {
            if ($request->query->get("linkAction") == "delete") {
                $categoriesTab = array();
                foreach ($test->getCategories() as $category) {
                    if ($category->getId() != $request->query->get("categoryId")) {
                        $categoriesTab[] = $category;
                    }
                }
                $test->setCategories($categoriesTab);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been deleted");

            }

        }

        return $this->render('CoreBundle:Manage:categories.html.twig', ["test" => $test]);
    }


    public function addCategoryAction(Request $request, $testId)
    {
        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);


        $Category = new Category();

        $form = $this->createForm(CategoryType::class, $Category);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $category = $form->getData();
                $test->addCategory($category);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been created");

                return $this->redirect($this->generateUrl("testmyskills_manage_categories", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }
        return $this->render('CoreBundle:Manage:addCategory.html.twig', ["test" => $test, "form" => $form->createView()]);
    }


    public function updateCategoryAction(Request $request, $testId, $categoryId)
    {

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);
        foreach ($test->getCategories() as $category) {
            if ($category->getId() == $categoryId) {
                break;
            }
        }

        $form = $this->createForm(CategoryType::class, $category);
        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $category = $form->getData();
                foreach ($test->getCategories() as $categoryTest) {
                    if ($categoryTest->getId() == $category->getId()) {
                        $categories[] = $category;
                    } else {
                        $categories[] = $categoryTest;
                    }
                }
                $test->setCategories($categories);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been updated");

                return $this->redirect($this->generateUrl("testmyskills_manage_categories", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }
        return $this->render('CoreBundle:Manage:addCategory.html.twig', ["test" => $test, "category" => $category, "form" => $form->createView()]);
    }


    public function usersAction()
    {
        return $this->render('CoreBundle:Manage:users.html.twig');
    }

    public function sluggify($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
        $slug = str_replace(array("\r", "\n"), '', $slug);
        return $slug;
    }
}