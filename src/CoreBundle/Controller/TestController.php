<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\TakenTest;
use CoreBundle\Document\TakenTestQuestion;
use CoreBundle\Entity\Test;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use CoreBundle\Form\Type\GuestType;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;

class TestController extends Controller
{

    public function cleanSession()
    {
        $session = new Session();
        $session->set("testQuestions", null);
        $session->set("takenTestId", null);
        $session->set("questionId", null);

    }

    public function newAction($slug = null)
    {
        $this->cleanSession();
        $itexamsUser = $this->get("itexams.user");
        $itexamsUser->checkCredit($this->getUser()->getId());

        if (!$this->get("itexams.user")->hasAPlan($this->getUser()->getId())) {
            return $this->redirect($this->generateUrl("testmyskills_prices"));
        } else {
            if ($slug == null) {
                $technologies = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findBy(array('activated' => true), ["order" => "desc"]);
                foreach ($technologies as $techno) {
                    $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByTechnologyId($techno->getId());
                    $count = 0;
                    foreach($tests as $test) {
                        if($test->getActivated() == true) {
                            $count++;
                        }
                    }
                    $number_tests[$techno->getSlug()] = $count;
                    $technos[$techno->getCategory()][] = $techno;
                }
                $technologies = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findBy(["activated" => true], ["order" => "desc"]);
                ksort($technos);
                $testsPersonal = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByUserId($this->getUser()->getId());
                $number_tests["personal"] = count($testsPersonal);
                return $this->render('CoreBundle:Test:new.html.twig', array(
                    'technologies' => $technos,
                    'number_tests' => $number_tests,
                    'viewTest' => false
                ));
            } else {
                if ($slug == "personal") {
                    $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByUserId($this->getUser()->getId());
                    $technology = new \StdClass();
                    $technology->name = "Personal tests";
                } else {
                    $technology = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Sector")->findBySlug($slug)[0];
                    $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByTechnologyId($technology->getId());
                }
                return $this->render('CoreBundle:Test:new.html.twig', array(
                    'tests' => $tests,
                    'viewTest' => true,
                    'technology' => $technology
                ));
            }
        }


    }


    public function newStep1Action($testSlug)
    {

        $session = new Session();
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findBynameSlugued($testSlug)[0];



        return $this->render('CoreBundle:Test:newstep1.html.twig', array(
            "test" => $test,
            "noInputName" => "false",
            "instruction" => $test->getInstructions(),

        ));
    }

    public function startTestAction(Request $request, $testSlug)
    {

        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByNameSlugued($testSlug);


        $testQuestions = $this->get("itexams.test")->retrieveQuestion($test[0]->getId());
        $questionId = array_shift($testQuestions);
        $questionData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Question")->find($questionId);


        $session = new Session();
        $dateStart = time();
        $dateEnd = time() + ($test[0]->getTime() * 60);


        $session->set("dateStart", $dateStart);
        $session->set("dateEnd", $dateEnd);

        // Save test in database
        if ($session->get("takenTestId") == null) {
            $takenTest = new TakenTest();
            $takenTest->setTestId($test[0]->getId());
            $takenTest->setUserId($this->getUser()->getId());
            $takenTest->setDate(new \DateTime());
            $takenTest->setTestName($request->request->get("name"));
            $itexamsUser = $this->get("itexams.user");
            $itexamsUser->decreaseCredit($this->getUser()->getId());


        } else {
            $takenTest = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenTest")->find($session->get("takenTestId"));
            $takenTest->setDate(new \DateTime());
        }

        $em = $this->get("doctrine_mongodb")->getManager();
        $em->persist($takenTest);
        $em->flush();


        $session = new Session();

        $session->set("testQuestions", $testQuestions);
        $session->set("takenTestId", $takenTest->getId());
        $session->set("questionId", $questionId);

        $instruction = $test[0]->getInstructions();
        $date = new \DateTime();

        $customisation = false;

        if ($takenTest->getGuestId() != null) {
            $customisation = true;
            $customisationData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());
            if($customisationData)  {
                $customisationData=$customisationData[0];
            }
        } else {
            $customisation = false;
            $customisationData= [];
        }




        return $this->render('CoreBundle:Test:starttest.html.twig', array(
            "test" => $test[0],
            "questionData" => $questionData,
            "dateStart" => $session->get("dateStart"),
            "dateEnd" => $session->get("dateEnd"),
            "customisation" => $customisation,
            "companyId" => $takenTest->getUserId(),
            "customisationData" => $customisationData

        ));
    }

    public function takeTestAction(Request $request, $testSlug)
    {
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByNameSlugued($testSlug)[0];

        $session = new Session();
        $dateStart = $session->get("dateStart");
        $dateEnd = $session->get("dateEnd");

        if ($dateEnd < time()) {
            return $this->redirectToRoute("testmyskills_dashboard_test_end", ["test_slug" => $testSlug], 301);
        }

        $takenTest = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenTest")->find($session->get("takenTestId"));

        if ($request->isMethod("post")) {


            $questionId = $session->get("questionId");
            $questionData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Question")->find($questionId);

            $takenTestQuestion = new TakenTestQuestion();
            $takenTestQuestion->setQuestion($questionData);
            $takenTestQuestion->setAnswers($request->request->get("answers"));

            $takenTest->addQuestion($takenTestQuestion);

            $em = $this->get("doctrine_mongodb")->getManager();
            $em->persist($takenTest);
            $em->flush();


            $testQuestions = $session->get("testQuestions");
            if (count($testQuestions) == 0) {
                return $this->redirect($this->generateUrl("testmyskills_dashboard_test_end", ["testSlug" => $test->getNameSlugued()]));
            }

            $questionId = array_shift($testQuestions);
            $questionData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Question")->find($questionId);

            $session->set("testQuestions", $testQuestions);
            $session->set("questionId", $questionId);

        }

        $customisation = false;
        if ($takenTest->getGuestId() != null) {
            $customisation = true;
        }
        $customisationData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());
        if($customisationData) {
            $customisationData = $customisationData[0];
        }


        return $this->render('CoreBundle:Test:taketest.html.twig', array(
            "test" => $test,
            "questionData" => $questionData,
            "dateStart" => $session->get("dateStart"),
            "dateEnd" => $session->get("dateEnd"),
            "customisation" => $customisation,
            "companyId" => $takenTest->getUserId(),
            "customisationData" => $customisationData
        ));
    }

    public function endAction($testSlug)
    {

        $session = new Session();
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByNameSlugued($testSlug)[0];
        $takenTest = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenTest")->find($session->get("takenTestId"));

        $this->get("itexams.test")->saveResults($session->get("takenTestId"));
        $results = $this->get("itexams.test")->retrieveDetailedResults($session->get("takenTestId"));

        $session->set("testQuestions", null);
        $session->set("takenTestId", null);
        $session->set("questionId", null);

        $customisation = false;
        if ($takenTest->getGuestId() != null) {
            $customisation = true;
        }
        $customisationData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());
        if($customisationData) {
            $customisation=$customisation[0];
        }


        return $this->render('CoreBundle:Test:end.html.twig', array(
            "test" => $test,
            "results" => $results,
            'noLayout' => false,
            "categories" => $results['categories'],
            "customisation" => $customisation,
            "companyId" => $takenTest->getUserId(),
            "customisationData" => $customisationData
        ));
    }

    public function inviteAction(Request $request)
    {

        $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findBy(['activated'=>true,'$or'=>[['userId' => $this->getUser()->getId()],['core' => true]]]);

        $itexams = $this->get("itexams.test");
        $itexamsUser = $this->get("itexams.user");

        $itexamsUser->checkCredit($this->getUser()->getId());


        $errors = [];

        $form = $this->createForm(GuestType::class, null, ["tests" => $tests]);

        $invitationSent = "false";

        if ($request->isMethod("post")) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
                if (!$itexamsUser->userExists($data["email"])) {
                    $result = $itexamsUser->createUser($data);
                    $user = $result['user'];
                    $password = $result['password'];
                } else {
                    $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->findByEmailCanonical(strtolower($data['email']))[0];

                }
                $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->find($data["test"]);

                // Save test in database
                $takenTest = new TakenTest();
                $takenTest->setTestId($test->getId());
                $takenTest->setUserId($this->getUser()->getId());
                $takenTest->setGuestId($user->getId());
                $takenTest->setDate(0);
                $takenTest->setTestName($data["firstname"] . " " . $data["lastname"]);

                $em = $this->get("doctrine_mongodb")->getManager();
                $em->persist($takenTest);
                $em->flush();
                $itexamsUser->decreaseCredit($this->getUser()->getId());
                $itexamsUser->inviteUser($user, $takenTest, $this->getUser());
                $invitationSent = "true";
            } else {
                $this->addFlash("error", $form->getErrors());
            }
        }
        return $this->render('CoreBundle:Test:invite.html.twig', array(
            "tests" => $tests,
            'form' => $form->createView(),
            'invitationSent' => $invitationSent
        ));
    }

    public function resultsDetailsAction($takenTestId)
    {

        $results = $this->get('itexams.test')->retrieveDetailedResults($takenTestId);
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->find($results['takenTest']->getTestId());

        $takenTest = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenTest")->find($takenTestId);


        return $this->render('CoreBundle:Test:resultDetails.html.twig', array(
            "test" => $test,
            "results" => $results,
            "noLayout" => false,
            "categories" => $results['categories'],

        ));
    }


    public function gueststartAction(Request $request, $takenTestId)
    {

        $takenTest = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenTest")->find($takenTestId);
        $customisation = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());


        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->find($takenTest->getTestId());

        $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($takenTest->getGuestId());

        // Here, "public" is the name of the firewall in your security.yml
        $token = new UsernamePasswordToken($user, $user->getPassword(), "public", $user->getRoles());

        // For older versions of Symfony, use security.context here
        $this->get("security.token_storage")->setToken($token);

        // Fire the login event
        // Logging the user in above the way we do it doesn't do this automatically
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $session = new Session();
        $dateStart = time();
        $dateEnd = time() + ($test->getTime() * 60);


        $session->set("dateStart", $dateStart);
        $session->set("dateEnd", $dateEnd);
        $session->set("takenTestId", $takenTestId);

        $customisation = false;
        if ($takenTest->getGuestId() != null) {
            $customisation = true;
        }
        $customisationData = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());
        if($customisationData)
        {
            $customisationData=$customisationData[0];
        }


        return $this->render('CoreBundle:Test:newstep1.html.twig', array(
            "test" => $test,
            "noInputName" => "true",
            "instruction" => $test->getInstructions(),
            "customisation" => $customisation,
            "companyId" => $takenTest->getUserId(),
            "customisationData" => $customisationData
        ));
    }

    public function manageAction()
    {
        $tests = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->findByUserId($this->getUser()->getId());
        return $this->render('CoreBundle:Test:manage.html.twig', ["tests" => $tests, "customisation" => $customisation]);
    }

    public function resultsAction()
    {


        $tests = $this->get("itexams.test")->retrieveUserTests($this->getUser()->getId());

        return $this->render('CoreBundle:Test:results.html.twig', array("tests" => $tests));
    }

    public function rateAction($type, $testId, $rate)
    {
        $test = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Test")->find($testId);
        $functionSet = "set" . ucfirst($type) . "Rate";
        $functionGet = "get" . ucfirst($type) . "Rate";
        if ($test->getTakenNumber() == 1) {
            $test->$functionSet($rate);
        } else {
            $test->$functionSet(($test->$functionGet() + $rate) / 2);
        }
        $em = $this->get("doctrine_mongodb")->getManager();
        $em->persist($test);
        $em->flush();
        die;
    }
}