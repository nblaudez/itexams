<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\Category;
use CoreBundle\Document\Credit;
use CoreBundle\Document\Exercice;
use CoreBundle\Document\Tag;
use CoreBundle\Document\Test;
use CoreBundle\Document\Question;
use CoreBundle\Form\Type\ExerciceType;
use CoreBundle\Form\Type\OrderType;
use CoreBundle\Form\Type\QuestionType;
use CoreBundle\Form\Type\CategoryType;
use CoreBundle\Form\Type\TestType;
use CoreBundle\Form\Type\TransactionType;
use CoreBundle\Form\Type\UserDetailsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller
{
    public function indexAction()
    {
        return $this->render('CoreBundle:Admin:index.html.twig');
    }


    public function exercicesAction() {
        $exercices =  $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findAll();
        return $this->render('CoreBundle:Admin:exercices.html.twig', ["exercices"=>$exercices]);
    }

    public function addExerciceAction(Request $request) {

        $form = $this->createForm(ExerciceType::class,new Exercice());

        if($request->isMethod("post")) {
            $form->handleRequest($request);
            if($form->isValid()) {

                $exercice = $form->getData();
                $exercice->setNameSlugued($this->sluggify($exercice->getName()));
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($exercice);
                $dm->flush();

                $this->addFlash("success", "Exercice has been added");
            }
        }

        return $this->render('CoreBundle:Admin:addExercice.html.twig', ["form"=>$form->createView()]);
    }

    public function updateExerciceAction(Request $request,$exerciceId) {

        $exercice = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Exercice")->find($exerciceId);

        $form = $this->createForm(ExerciceType::class,$exercice);

        if($request->isMethod("post")) {
            $form->handleRequest($request);
            if($form->isValid()) {

                $exercice = $form->getData();
                $exercice->setNameSlugued($this->sluggify($exercice->getName()));
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($exercice);
                $dm->flush();

                $this->addFlash("success", "Exercice has been updated");
            }
        }

        return $this->render('CoreBundle:Admin:updateExercice.html.twig', ["form"=>$form->createView()]);
    }

    public function testsAction(Request $request)
    {


        if ($request->query->get("linkAction") == "delete") {
            if ($request->query->get("linkAction") == "delete") {

                $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($request->query->get("testId"));

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->remove($test);
                $dm->flush();

                $this->addFlash("success", "Test has been deleted");

            }

        }

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        if(in_array("ROLE_SUPER_ADMIN", $this->getUser()->getRoles())) {
            $tests = $test->findAll();
        } else {
            $tests = $test->findByWriterId($this->getUser()->getId());
        }


        return $this->render('CoreBundle:Admin:tests.html.twig', ["tests" => $tests]);

    }

    public function testUpdateAction(Request $request, $testId)
    {


        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $test = $test->find($testId);

        $form = $this->createForm(TestType::class, $test);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $test = $form->getData();
                $test->setWriterId($this->getUser()->getId());
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Test has been updated");

                return $this->redirect($this->generateUrl("testmyskills_admin_tests"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:Admin:testUpdate.html.twig', ["test" => $test, "form" => $form->createView()]);

    }


    public function testAddAction(Request $request)
    {

        $test = new Test();
        $form = $this->createForm(TestType::class, $test);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $test = $form->getData();
                for($i=1;$i<=10;$i++) {
                    $results[]=["value"=>$i*10,"result"=>"0"];
                }
                $test->setResults($results);
                $test->setNameSlugued($this->sluggify($test->getName()));
                $test->setCore(true);
                $tags = explode(",", $test->getTags());
                $test->setTags($tags);

                $tags=$test->getTags();
                $tags=explode(",",$tags);
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                    }
                }
                $test->setWriterId($this->getUser()->getId());
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Test has been created");

                return $this->redirect($this->generateUrl("testmyskills_admin_tests"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        return $this->render('CoreBundle:Admin:testAdd.html.twig', array('form' => $form->createView()));
    }


    public function testQuestionsAction(Request $request, $testId)
    {

        $testRepository = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $test = $testRepository->find($testId);

        $questionRepository = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Question");
        $questions=$questionRepository->findByTestId($testId);

        if ($request->query->get("linkAction") == "delete") {

            $question = $questionRepository->find($request->query->get("questionId"));
            $dm = $this->get('doctrine_mongodb')->getManager();
            $dm->remove($question);
            $dm->flush();

            $this->addFlash("success", "Question has been deleted");

        }
        return $this->render('CoreBundle:Admin:testQuestions.html.twig', ["test" => $test,'questions'=>$questions]);

    }

    public function testQuestionUpdateAction(Request $request, $testId, $questionId)
    {

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);
        $question = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Question")->find($questionId);

        $tagString='';
        $tags=$question->getTags();
        foreach($tags as $tag) {
            $tagString.=$tag->getName().",";
        }
        $tagString=rtrim($tagString,",");
        foreach ($test->getCategories() as $category) {
            $cat = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Category")->find($category->getId());
            $categories[$cat->getId()] = $cat->getName();
        }

        $form = $this->createForm(QuestionType::class, $question, ["tags"=>$tagString,"categories" => $categories]);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            if ($form->isValid()) {

                $question = $form->getData();
                $question->setTestId($testId);


                $question->setCategoryId($question->getCategoryId());


                $tags=$question->getTags();
                $tags=explode(",",$tags);
                $question->cleanTags();
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $question->addTag($mTag);


                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                    }
                }


                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($question);
                $dm->flush();

                $this->addFlash("success", "Question has been updated");

                return $this->redirect($this->generateUrl("testmyskills_admin_test_questions", ["testId" => $test->getId()]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:Admin:testQuestionUpdate.html.twig', ["form" => $form->createView(), "test" => $test]);

    }

    public function testQuestionAddAction(Request $request, $testId)
    {
        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);
        $question = new Question();
        foreach ($test->getCategories() as $category) {
            $cat = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Category")->find($category->getId());
            $categories[$cat->getId()] = $cat->getName();
        }
        $form = $this->createForm(QuestionType::class, $question, ["categories" => $categories]);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $question = $form->getData();
                $question->setTestId($testId);

                $category=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Category")->find($question->getCategoryId());
                $question->setCategoryId($category);

                $tags=$question->getTags();
                $tags=explode(",",$tags);
                $cTags=[];
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                        $cTags[]=$mTag;
                    }
                }
                $question->setTags($cTags);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($question);
                $dm->flush();

                $this->addFlash("success", "Question has been created");

                return $this->redirect($this->generateUrl("testmyskills_admin_test_questions", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:Admin:testQuestionAdd.html.twig', ["test" => $test, "form" => $form->createView()]);
    }


    public function testCategoriesAction(Request $request, $testId)
    {
        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);

        if ($request->query->get("linkAction") == "delete") {
            if ($request->query->get("linkAction") == "delete") {
                $categoriesTab = array();
                foreach ($test->getCategories() as $category) {
                    if ($category->getId() != $request->query->get("categoryId")) {
                        $categoriesTab[] = $category;
                    }
                }
                $test->setCategories($categoriesTab);

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been deleted");

            }

        }

        return $this->render('CoreBundle:Admin:testCategories.html.twig', ["test" => $test]);
    }


    public function testCategoryAddAction(Request $request, $testId)
    {
        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);


        $Category = new Category();

        $form = $this->createForm(CategoryType::class, $Category);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $category = $form->getData();
                $test->addCategory($category);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been created");

                return $this->redirect($this->generateUrl("testmyskills_admin_test_categories", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }
        return $this->render('CoreBundle:Admin:testCategoryAdd.html.twig', ["test" => $test, "form" => $form->createView()]);
    }


    public function testCategoryUpdateAction(Request $request, $testId, $categoryId)
    {

        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($testId);

        foreach ($test->getCategories() as $category) {
            if ($category->getId() == $categoryId) {
                break;
            }
        }

        $form = $this->createForm(CategoryType::class, $category);
        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                $category = $form->getData();
                foreach ($test->getCategories() as $categoryTest) {
                    if ($categoryTest->getId() == $category->getId()) {
                        $categories[] = $category;
                    } else {
                        $categories[] = $categoryTest;
                    }
                }
                $test->setCategories($categories);
                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Category has been updated");

                return $this->redirect($this->generateUrl("testmyskills_admin_test_categories", ["testId" => $testId]));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }
        return $this->render('CoreBundle:Admin:testCategoryUpdate.html.twig', ["test" => $test, "category" => $category, "form" => $form->createView()]);
    }


    public function usersAction()
    {
        $users = $this->get('doctrine_mongodb')->getRepository("CoreBundle:User")->findAll();
        return $this->render('CoreBundle:Admin:users.html.twig',["users"=>$users]);
    }

    public function userDetailsAction(Request $request, $userId) {

        $user = $this->get('doctrine_mongodb')->getRepository("CoreBundle:User")->find($userId);
        $transactions = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Order")->findByUserId($userId);
        $form = $this->createForm(UserDetailsType::class,$user);
        $credits = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Credit")->findByUserId($userId);
        if($credits) {
            $credits = $credits[0];
        } else {
            $credits = new Credit();
            $credits->setUserId($userId);
        }

        if($request->isMethod("post")) {
            if($request->get('form_action')=="Update credits") {


                $credits->setCredits($request->get('credits'));

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($credits);
                $dm->flush();
            } else {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $user = $form->getData();

                    $userManager = $this->get('fos_user.user_manager');
                    $userManager->updateUser($user);

                    $this->addFlash("success", "User has been updated");

                }
            }
        }


        return $this->render('CoreBundle:Admin:userDetails.html.twig',["credits"=>$credits,"user"=>$user,"form"=>$form->createView(),"transactions"=>$transactions]);
    }

    public function orderDetailsAction(Request $request, $orderId) {
        $order = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Order")->find($orderId);
        $form = $this->createForm(OrderType::class, $order);
        if($request->isMethod("post")) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $order = $form->getData();
                $dm = $this->get('doctrine_mongodb')->getManager();
                $this->addFlash("success", "Order has been updated");
                $dm->persist($order);
                $dm->flush();
            }
        }
        $user = $this->get('doctrine_mongodb')->getRepository("CoreBundle:User")->find($order->getUserId());
        return $this->render('CoreBundle:Admin:orderDetails.html.twig',["order"=>$order, "user"=>$user,"form"=>$form->createView()]);
    }

    public function sluggify($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
        $slug = str_replace(array("\r", "\n"), '', $slug);
        return $slug;
    }

   
}