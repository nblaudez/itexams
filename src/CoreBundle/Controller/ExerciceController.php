<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\Question;
use CoreBundle\Document\TakenExercice;
use CoreBundle\Document\TakenExerciceQuestion;
use CoreBundle\Form\Type\GuestExerciceType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class ExerciceController extends Controller
{
    public function newAction()
    {
        $itexamsUser = $this->get("itexams.user");
        $itexamsUser->checkCredit($this->getUser()->getId());

        $exercices = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findBy(array('activated' => true),["order"=>"desc"]);
        return $this->render('CoreBundle:Exercice:new.html.twig', ['exercices'=>$exercices]);
    }

    public function newStep1Action($exerciceSlug)
    {

        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findBynameSlugued($exerciceSlug)[0];

        return $this->render('CoreBundle:Exercice:newstep1.html.twig', array(
            "exercice" => $exercice,
            "noInputName"=>"false",
            "instruction" => $exercice->getInstructions()
        ));
    }

    public function startAction(Request $request, $exerciceSlug)
    {


        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findByNameSlugued($exerciceSlug)[0];
        $questionsData = $exercice->getQuestions();

        $session = new Session();
        $dateStart=time();
        $dateEnd=time()+($exercice->getTime()*60);


        $session->set("dateStart",$dateStart);
        $session->set("dateEnd",$dateEnd);


        if($dateEnd < time()) {
            return $this->redirectToRoute("testmyskills_dashboard_exercice_end",["exercice_slug"=>$exerciceSlug],301);
        }


        // Save test in database
        if($session->get("takenExerciceId") == null) {
            $takenExercice = new TakenExercice();
            $takenExercice->setExerciceId($exercice->getId());
            $takenExercice->setUserId($this->getUser()->getId());
            $takenExercice->setDate(new \DateTime());
            $takenExercice->setExerciceName($request->request->get("name"));
            $takenExercice->setLanguage($request->request->get("language"));
            $questions=[];
            foreach($questionsData as $questionData) {
                $question = new Question();
                $question->setQuestion($questionData->getQuestion());
                $questions[]=$question;
            }
            $takenExercice->setQuestions($question);
        } else {
            $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($session->get("takenExerciceId"));
            $takenExercice->setDate(new \DateTime());
        }

        $em = $this->get("doctrine_mongodb")->getManager();
        $em->persist($takenExercice);
        $em->flush();



        $session = new Session();

        $session->set("exerciceQuestions", $exercice);
        $session->set("takenExerciceId", $takenExercice->getId());
        $session->set("questionId", 0);



        $instruction = $exercice->getInstructions();
        $date = new \DateTime();
        return $this->render('CoreBundle:Exercice:start.html.twig', array(
            "exercice" => $exercice,
            "questionsData" => $questionsData,
            "dateStart"=>$session->get("dateStart"),
            "dateEnd"=>$session->get("dateEnd"),
            "takenExercice"=>$takenExercice
        ));
    }


    public function inviteAction(Request $request)
    {
        $exercices = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->findAll();

        $itexams = $this->get("itexams.test");
        $itexamsUser = $this->get("itexams.user");
        $errors = [];

        $itexamsUser->checkCredit($this->getUser()->getId());

        $form = $this->createForm(GuestExerciceType::class, null, ["exercices" => $exercices]);

        $invitationSent = "false";

        if ($request->isMethod("post")) {
            $form->handleRequest($request);
            if ($form->isValid()) {


                $itexamsUser->decreaseCredit($this->getUser()->getId());
                $data = $form->getData();
                if (!$itexamsUser->userExists($data["email"])) {
                    $result = $itexamsUser->createUser($data);
                    $user = $result['user'];
                    $password = $result['password'];
                } else {
                    $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->findByEmailCanonical(strtolower($data['email']))[0];

                }
                $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->find($data["exercice"]);

                // Save test in database
                $takenExercice = new TakenExercice();
                $takenExercice->setExerciceId($exercice->getId());
                $takenExercice->setUserId($this->getUser()->getId());
                $takenExercice->setGuestId($user->getId());
                $takenExercice->setDate(0);
                $takenExercice->setExerciceName($data["firstname"] . " " . $data["lastname"]);
                $takenExercice->setLanguage($data["language"]);

                foreach($exercice->getQuestions() as $questionData) {
                    $question = new Question();
                    $question->setQuestion($questionData["question"]);
                    $takenExercice->addQuestion($question);
                }

                $em=$this->get("doctrine_mongodb")->getManager();
                $em->persist($takenExercice);
                $em->flush();

                $itexamsUser->inviteExerciceUser($user, $takenExercice, $this->getUser());
                $invitationSent = "true";
            } else {
                $this->addFlash("error", $form->getErrors());
            }
        }
        return $this->render('CoreBundle:Exercice:invite.html.twig', array(
            "exercices" => $exercices,
            'form' => $form->createView(),
            'invitationSent' => $invitationSent
        ));
    }


    public function detailsAction(Request $request, $takenExerciceId) {

        $session = new Session();
        $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($takenExerciceId);
        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId());

        $finder = new Finder();
        $finder->files()->in(__DIR__."/../../../web/UploadedFiles/exercices/".$takenExercice->getId()."/");

        foreach ($finder as $file) {
            $files[]=$file->getRelativePathname();
        }

        if($request->isMethod("post")) {
            if($request->get("form_action")=="Note") {
                $graded = false;
                $allGraded=true;
                $average=0;
                $i = 0;
                foreach($takenExercice->getQuestions() as $question) {
                    if($i == $request->get("questionId")) {
                        $question->setGrade($request->get("note"));
                    }
                    if($question->getGrade() == null) {
                        $allGraded=false;
                    }

                    $average +=$question->getGrade();

                    $em = $this->get("doctrine_mongodb")->getManager();
                    $em->persist($takenExercice);
                    $em->flush();

                    $graded=true;
                    $i++;
                }
                $average = $average / count($takenExercice->getQuestions());

                if($allGraded) {
                    $takenExercice->setResult($average);
                    $em = $this->get("doctrine_mongodb")->getManager();
                    $em->persist($takenExercice);
                    $em->flush();
                }

                if($graded) {
                    $this->addFlash("success","Question has been graded");
                }

                $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($takenExerciceId);
            }
        }


        return $this->render('CoreBundle:Exercice:details.html.twig', array(
            "exercice" => $exercice,
            "takenExercice" => $takenExercice,
            "files" => $files
        ));
    }

    public function endAction(Request $request, $takenExerciceId)
    {

        $session = new Session();
        $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($takenExerciceId);
        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId());


        $session->set("testQuestions", null);
        $session->set("takenTestId", null);
        $session->set("questionId", null);


        return $this->render('CoreBundle:Exercice:end.html.twig', array(
            "exercice" => $exercice,
            "takenExercice" => $takenExercice
        ));
    }

    public function questionDetailsAction($takenExerciceId, $questionId) {

        $session = new Session();
        $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($takenExerciceId);
        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId());


        return $this->render('CoreBundle:Exercice:questionDetails.html.twig', array(
            "exercice" => $exercice,
            "takenExercice" => $takenExercice,
            "questionId" => $questionId,

        ));
    }


    public function resultsAction()
    {

        $exercices = $this->get("itexams.exercice")->retrieveUserExercice($this->getUser()->getId());

        return $this->render('CoreBundle:Exercice:results.html.twig', array("exercices" => $exercices));
    }

    public function gueststartAction(Request $request, $takenexerciceId) {
        $takenExercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:TakenExercice")->find($takenexerciceId);
        $exercice = $this->get("doctrine_mongodb")->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId());

        $user = $this->get("doctrine_mongodb")->getRepository("CoreBundle:User")->find($takenExercice->getGuestId());

        // Here, "public" is the name of the firewall in your security.yml
        $token = new UsernamePasswordToken($user, $user->getPassword(), "public", $user->getRoles());

        // For older versions of Symfony, use security.context here
        $this->get("security.token_storage")->setToken($token);

        // Fire the login event
        // Logging the user in above the way we do it doesn't do this automatically
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        $session = new Session();
        $dateStart=time();
        $dateEnd=time()+($exercice->getTime()*60);


        $session->set("dateStart",$dateStart);
        $session->set("dateEnd",$dateEnd);
        $session->set("takenExerciceId",$takenexerciceId);

        return $this->render('CoreBundle:Exercice:newstep1.html.twig', array(
            "exercice" => $exercice,
            "noInputName"=>"true",
            "instruction" => $exercice->getInstructions()
        ));
    }
}