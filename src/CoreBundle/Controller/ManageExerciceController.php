<?php

namespace CoreBundle\Controller;

use CoreBundle\Document\Category;
use CoreBundle\Document\Exercice;
use CoreBundle\Document\Tag;
use CoreBundle\Document\Test;
use CoreBundle\Document\Question;
use CoreBundle\Form\Type\ExerciceType;
use CoreBundle\Form\Type\QuestionType;
use CoreBundle\Form\Type\CategoryType;
use CoreBundle\Form\Type\TestType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ManageExerciceController extends Controller
{
    public function indexAction(Request $request)
    {
        if ($request->query->get("linkAction") == "delete") {
            if ($request->query->get("linkAction") == "delete") {

                $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test")->find($request->query->get("testId"));

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->remove($test);
                $dm->flush();

                $this->addFlash("success", "Test has been deleted");

            }

        }

        $exercice = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Exercice");
        $exercice = $exercice->findByUserId($this->getUser()->getId());


        return $this->render('CoreBundle:ManageExercice:index.html.twig', ["exercice" => $exercice]);

    }



    public function updateAction(Request $request, $testId)
    {


        $test = $this->get('doctrine_mongodb')->getRepository("CoreBundle:Test");
        $test = $test->find($testId);

        $form = $this->createForm(TestType::class, $test);

        if ($request->isMethod("post")) {

            $form->handleRequest($request);

            if ($form->isValid()) {
                $test = $form->getData();


                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($test);
                $dm->flush();

                $this->addFlash("success", "Test has been updated");

                return $this->redirect($this->generateUrl("testmyskills_manage_tests"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }


        return $this->render('CoreBundle:ManageExercice:update.html.twig', ["test" => $test, "form" => $form->createView()]);

    }


    public function addAction(Request $request)
    {

        $exercice = new Exercice();
        $form = $this->createForm(ExerciceType::class, $exercice);

        if ($request->isMethod("post")) {
            $form->handleRequest($request);

            if ($form->isValid()) {
                for($i=0;$i<=10;$i++) {
                    $results[]=["value"=>$i*10,"result"=>"0"];
                }
                $exercice = $form->getData();
                $exercice->setResults($results);
                $exercice->setNameSlugued($this->sluggify($exercice->getName()));
                $exercice->setCore(0);
                $tags = explode(",", $exercice->getTags());
                $exercice->setTags($tags);
                $exercice->setUserId($this->getUser()->getId());
                $exercice->setCore(0);
                $exercice->setNumberQuestions(count($exercice->getQuestions()));
                $tags=$exercice->getTags();
                $tags=explode(",",$tags);
                foreach($tags as $tag) {
                    $dbtag=$this->get('doctrine_mongodb')->getRepository("CoreBundle:Tag")->findByName($tag);
                    if($dbtag==null) {
                        $mTag=new Tag();
                        $mTag->setName($tag);

                        $dm = $this->get('doctrine_mongodb')->getManager();
                        $dm->persist($mTag);
                        $dm->flush();
                    }
                }

                $dm = $this->get('doctrine_mongodb')->getManager();
                $dm->persist($exercice);
                $dm->flush();

                $this->addFlash("success", "Exercice has been created");

                return $this->redirect($this->generateUrl("testmyskills_manage_exercices"));

            } else {
                $this->addFlash("error", $form->getErrors());
            }

        }

        return $this->render('CoreBundle:ManageExercice:add.html.twig', array('form' => $form->createView()));
    }



    public function sluggify($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
        $slug = str_replace(array("\r", "\n"), '', $slug);
        return $slug;
    }
}