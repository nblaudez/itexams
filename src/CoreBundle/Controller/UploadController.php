<?php
namespace CoreBundle\Controller;

use \Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Intl\Tests\Data\Provider\Json\JsonRegionDataProviderTest;


class UploadController extends Controller
{
    public function exercicesAction(Request $request, $takenExerciceId)
    {
        foreach($request->files as $file) {
            if($file->move(__DIR__."/../../../www/UploadedFiles/exercices/$takenExerciceId/",$file->getclientOriginalName())) {
                return new JsonResponse(["success"=>true],200);
            } else {
                return new JsonResponse(["success"=>false],500);
            }
        }
        return new JsonResponse(["success"=>true],200);
    }

    public function customisationAction(Request $request, $userId) {
        foreach($request->files as $file) {
            if($file->move(__DIR__."/../../../www/UploadedFiles/logo/$userId/","logo.png")) {
                return new JsonResponse(["success"=>true],200);
            } else {
                return new JsonResponse(["success"=>false],500);
            }
        }
        return new JsonResponse(["success"=>true],200);
    }
}