<?php

namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Withdrawal
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $îd;
    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     * @MongoDB\Date
     */
    protected $date;
    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $amount;
    /**
     *  @MongoDB\Field(type="string")
     */
    protected $status;

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getÎd()
    {
        return $this->îd;
    }

    /**
     * @param mixed $îd
     */
    public function setÎd($îd)
    {
        $this->îd = $îd;
    }



    /**
     * Get îd
     *
     * @return id $îd
     */
    public function getîd()
    {
        return $this->îd;
    }
}
