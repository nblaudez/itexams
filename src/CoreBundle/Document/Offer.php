<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Offer
{

    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;


    /**
     *  @MongoDB\Field(type="float")
     */
    protected $price;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $active;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $numberOfTests;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Set numberOfTests
     *
     * @param int $numberOfTests
     * @return self
     */
    public function setNumberOfTests($numberOfTests)
    {
        $this->numberOfTests = $numberOfTests;
        return $this;
    }

    /**
     * Get numberOfTests
     *
     * @return int $numberOfTests
     */
    public function getNumberOfTests()
    {
        return $this->numberOfTests;
    }
}
