<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class TakenTestQuestion
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;


    /**
     * @MongoDB\EmbedOne(targetDocument="Question")
     */
    protected $question;

    /**
     * @MongoDB\Hash
     */
    protected $answers;




    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param CoreBundle\Document\Question $question
     * @return self
     */
    public function setQuestion(\CoreBundle\Document\Question $question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * Get question
     *
     * @return CoreBundle\Document\Question $question
     */
    public function getQuestion()
    {
        return $this->question;
    }



    /**
     * Set answers
     *
     * @param collection $answers
     * @return self
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * Get answers
     *
     * @return collection $answers
     */
    public function getAnswers()
    {
        return $this->answers;
    }
}
