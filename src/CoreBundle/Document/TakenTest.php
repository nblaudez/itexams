<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class TakenTest
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $testId;



    /**
     *  @MongoDB\Field(type="string")
     */
    protected $testName;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userType;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userId;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $guestId;

    /**
     * @MongoDB\EmbedMany(targetDocument="TakenTestQuestion")
     */
    protected $questions;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $result;

    /**
     * @MongoDB\Date
     */
    protected $date;


    /**
     * @MongoDB\Collection
     */
    protected $categoriesResult;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $sitePercentage;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $rang;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $duration;

    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set guestId
     *
     * @param String $guestId
     * @return self
     */
    public function setGuestId($guestId)
    {
        $this->guestId = $guestId;
        return $this;
    }

    /**
     * Get guestId
     *
     * @return string $guestId
     */
    public function getGuestId()
    {
        return $this->guestId;
    }

    /**
     * Set testId
     *
     * @param String $testId
     * @return self
     */
    public function setTestId($testId)
    {
        $this->testId = $testId;
        return $this;
    }

    /**
     * Get testId
     *
     * @return string $testId
     */
    public function getTestId()
    {
        return $this->testId;
    }

    /**
     * Set testName
     *
     * @param string $testName
     * @return self
     */
    public function setTestName($testName)
    {
        $this->testName = $testName;
        return $this;
    }

    /**
     * Get testName
     *
     * @return string $testName
     */
    public function getTestName()
    {
        return $this->testName;
    }

    /**
     * Set userType
     *
     * @param string $userType
     * @return self
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * Get userType
     *
     * @return string $userType
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Add question
     *
     * @param CoreBundle\Document\TakenTestQuestion $question
     */
    public function addQuestion(\CoreBundle\Document\TakenTestQuestion $question)
    {
        $this->questions[] = $question;
    }

    /**
     * Remove question
     *
     * @param CoreBundle\Document\TakenTestQuestion $question
     */
    public function removeQuestion(\CoreBundle\Document\TakenTestQuestion $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection $questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set result
     *
     * @param int $result
     * @return self
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * Get result
     *
     * @return int $result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getCategoriesResult()
    {
        return $this->categoriesResult;
    }

    /**
     * @param mixed $categoriesResult
     */
    public function setCategoriesResult($categoriesResult)
    {
        $this->categoriesResult = $categoriesResult;
    }

    /**
     * @return mixed
     */
    public function getSitePercentage()
    {
        return $this->sitePercentage;
    }

    /**
     * @param mixed $sitePercentage
     */
    public function setSitePercentage($sitePercentage)
    {
        $this->sitePercentage = $sitePercentage;
    }

    /**
     * Set rang
     *
     * @param int $rang
     * @return self
     */
    public function setRang($rang)
    {
        $this->rang = $rang;
        return $this;
    }

    /**
     * Get rang
     *
     * @return int $rang
     */
    public function getRang()
    {
        return $this->rang;
    }

    /**
     * Set duration
     *
     * @param string $duration
     * @return self
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
        return $this;
    }

    /**
     * Get duration
     *
     * @return string $duration
     */
    public function getDuration()
    {
        return $this->duration;
    }
}
