<?php

namespace CoreBundle\Document;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class User extends BaseUser
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $facebookId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $gplusId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $linkedinId;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $companyName;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $lastname;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $firstname;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $address;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $zipCode;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $city;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $country;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $affiliateId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $iban;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $swift;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $vendorId;


    public function __construct() {

        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }


    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }



    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param mixed $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    public function setEmail($email) {
        $this->setUsername($email);
        parent::setEmail($email);
    }



    /**
     * Set facebookId
     *
     * @param string $facebookId
     * @return self
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * Get facebookId
     *
     * @return string $facebookId
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * Set gplusId
     *
     * @param string $gplusId
     * @return self
     */
    public function setGplusId($gplusId)
    {
        $this->gplusId = $gplusId;
        return $this;
    }

    /**
     * Get gplusId
     *
     * @return string $gplusId
     */
    public function getGplusId()
    {
        return $this->gplusId;
    }


    /**
     * Set linkedinId
     *
     * @param string $linkedinId
     * @return self
     */
    public function setLinkedinId($linkedinId)
    {
        $this->linkedinId = $linkedinId;
        return $this;
    }

    /**
     * Get linkedinId
     *
     * @return string $linkedinId
     */
    public function getLinkedinId()
    {
        return $this->linkedinId;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function hasUserInfos() {
        if($this->getFirstname() != "" && $this->getLastname() != "" && $this->getCompanyName() != "" &&
            $this->getCountry() != "" && $this->getCity() != "" &&
            $this->getZipCode() != "")
        {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return mixed
     */
    public function getAffiliateId()
    {
        return $this->affiliateId;
    }

    /**
     * @param mixed $affiliateId
     */
    public function setAffiliateId($affiliateId)
    {
        $this->affiliateId = $affiliateId;
    }

    /**
     * @return mixed
     */
    public function getSwift()
    {
        return $this->swift;
    }

    /**
     * @param mixed $swift
     */
    public function setSwift($swift)
    {
        $this->swift = $swift;
    }

    /**
     * @return mixed
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * @param mixed $iban
     */
    public function setIban($iban)
    {
        $this->iban = $iban;
    }

    /**
     * @return mixed
     */
    public function getVendorId()
    {
        return $this->vendorId;
    }

    /**
     * @param mixed $vendorId
     */
    public function setVendorId($vendorId)
    {
        $this->vendorId = $vendorId;
    }
}
