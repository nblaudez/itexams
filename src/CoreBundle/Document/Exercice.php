<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Exercice
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $name;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $nameSlugued;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $instructions;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $writerId;

    /**
     * @MongoDB\Collection
     */
    private $tags = array();


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $content;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $time;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $numberQuestions;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $activated;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $core;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $basic;

    /**
     * @MongoDB\Collection
     */
    protected $questions;

    /**
     * @MongoDB\Collection
     */
    protected $results;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $takenNumber;




    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();

    }

    /**
     * @return mixed
     */
    public function getTakenNumber()
    {
        return $this->takenNumber;
    }

    /**
     * @param mixed $takenNumber
     */
    public function setTakenNumber($takenNumber)
    {
        $this->takenNumber = $takenNumber;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param mixed $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }

    /**
     * @return mixed
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * @param mixed $questions
     */
    public function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    /**
     * @return mixed
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param mixed $basic
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * @param mixed $activated
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
    }

    /**
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @param mixed $time
     */
    public function setTime($time)
    {
        $this->time = $time;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getWriterId()
    {
        return $this->writerId;
    }

    /**
     * @param mixed $writerId
     */
    public function setWriterId($writerId)
    {
        $this->writerId = $writerId;
    }

    /**
     * @return mixed
     */
    public function getNameSlugued()
    {
        return $this->nameSlugued;
    }

    /**
     * @param mixed $nameSlugued
     */
    public function setNameSlugued($nameSlugued)
    {
        $this->nameSlugued = $nameSlugued;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * @param mixed $instructions
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
    }

    /**
     * @return mixed
     */
    public function getNumberQuestions()
    {
        return $this->numberQuestions;
    }

    /**
     * @param mixed $numberQuestions
     */
    public function setNumberQuestions($numberQuestions)
    {
        $this->numberQuestions = $numberQuestions;
    }

    /**
     * @return mixed
     */
    public function getCore()
    {
        return $this->core;
    }

    /**
     * @param mixed $core
     */
    public function setCore($core)
    {
        $this->core = $core;
    }


    /**
     * Set content
     *
     * @param string $content
     * @return self
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    /**
     * Get content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }


}
