<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Test
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

   
    /**
     *  @MongoDB\Field(type="string")
     */
    protected $sectorId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $name;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $sectorSlug;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $nameSlugued;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $description;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $instructions;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $writerId;

    /**
     * @MongoDB\Collection
     */
    private $tags = array();


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $time;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $numberQuestions;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $activated;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $core;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userId;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $basic;

    /**
     * @MongoDB\EmbedMany(targetDocument="Category")
     */
    protected $categories;

    /**
     * @MongoDB\Collection
     */
    protected $results;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $takenNumber;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $pertinenceRate;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $difficultyRate;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $timeRate;


    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set number of question
     *
     * @param string $numberQuestions
     * @return self
     */
    public function setNumberQuestions($numberQuestions)
    {
        $this->numberQuestions = $numberQuestions;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getNumberQuestions()
    {
        return $this->numberQuestions;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set instructions
     *
     * @param string $instructions
     * @return self
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;
        return $this;
    }

    /**
     * Get instructions
     *
     * @return string $instructions
     */
    public function getInstructions()
    {
        return $this->instructions;
    }

    /**
     * Set tags
     *
     * @param collection $tags
     * @return self
     */
    public function setTags($tags)
    {
        //$tags=explode(",",$tags);
        $this->tags = $tags;
        return $this;
    }

    /**
     * Get tags
     *
     * @return collection $tags
     */
    public function getTags()
    {
        $tagsString = "";
        if (method_exists($this->tags, "count") && $this->tags->count() > 0) {
            foreach ($this->tags as $tag)
                $tagsString .= $tag . ",";
            $tagsString = rtrim($tagsString, ",");
        }
        return $tagsString;
    }


    /**
     * Set time
     *
     * @param int $time
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;
        return $this;
    }

    /**
     * Get time
     *
     * @return int $time
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set activated
     *
     * @param bool $activated
     * @return self
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
        return $this;
    }

    /**
     * Get activated
     *
     * @return bool $activated
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set core
     *
     * @param bool $core
     * @return self
     */
    public function setCore($core)
    {
        $this->core = $core;
        return $this;
    }

    /**
     * Get core
     *
     * @return bool $core
     */
    public function getCore()
    {
        return $this->core;
    }


    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }


    /**
     * Add category
     *
     * @param CoreBundle\Document\Category $category
     */
    public function addCategory(\CoreBundle\Document\Category $category)
    {
        $this->categories[] = $category;
    }

    /**
     * Remove category
     *
     * @param CoreBundle\Document\Category $category
     */
    public function removeCategory(\CoreBundle\Document\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Get categories
     *
     * @return \Doctrine\Common\Collections\Collection $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * Set Categories
     *
     * @param Array $categories
     * @return self
     */
    public function setCategories($categories)
    {
        return $this->categories = $categories;
    }


    public function getNumberOfQuestions()
    {
        $nbQuestions = 0;
        foreach ($this->categories as $category) {
            $nbQuestions += $category->getNumberOfQuestions();
        }
        return $nbQuestions;
    }

    /**
     * @return mixed
     */
    public function getNameSlugued()
    {
        return $this->nameSlugued;
    }

    /**111
     * @param mixed $namedSlug
     */
    public function setNameSlugued($nameSlugued)
    {
        $this->nameSlugued = $nameSlugued;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }



    /**
     * @return bool
     */
    public function getBasic()
    {
        return $this->basic;
    }

    /**
     * @param bool $basic
     */
    public function setBasic($basic)
    {
        $this->basic = $basic;
    }

    /**
     * @return mixed
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param mixed $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }


    /**
     * Set takenNumber
     *
     * @param int $takenNumber
     * @return self
     */
    public function setTakenNumber($takenNumber)
    {
        $this->takenNumber = $takenNumber;
        return $this;
    }

    /**
     * Get takenNumber
     *
     * @return int $takenNumber
     */
    public function getTakenNumber()
    {
        return $this->takenNumber;
    }

    /**
     * @return mixed
     */
    public function getWriterId()
    {
        return $this->writerId;
    }

    /**
     * @param mixed $writerId
     */
    public function setWriterId($writerId)
    {
        $this->writerId = $writerId;
    }

    /**
     * @return mixed
     */
    public function getPertinenceRate()
    {
        return $this->pertinenceRate;
    }

    /**
     * @param mixed $pertinenceRate
     */
    public function setPertinenceRate($pertinenceRate)
    {
        $this->pertinenceRate = $pertinenceRate;
    }

    /**
     * @return mixed
     */
    public function getDifficultyRate()
    {
        return $this->difficultyRate;
    }

    /**
     * @param mixed $difficultyRate
     */
    public function setDifficultyRate($difficultyRate)
    {
        $this->difficultyRate = $difficultyRate;
    }

    /**
     * @return mixed
     */
    public function getTimeRate()
    {
        return $this->timeRate;
    }

    /**
     * @param mixed $timeRate
     */
    public function setTimeRate($timeRate)
    {
        $this->timeRate = $timeRate;
    }

    /**
     * @return mixed
     */
    public function getsectorId()
    {
        return $this->sectorId;
    }

    /**
     * @param mixed $sectorId
     */
    public function setsectorId($sectorId)
    {
        $this->sectorId = $sectorId;
    }

    /**
     * @return mixed
     */
    public function getsectorSlug()
    {
        return $this->sectorSlug;
    }

    /**
     * @param mixed $sectorSlug
     */
    public function setsectorSlug($sectorSlug)
    {
        $this->sectorSlug = $sectorSlug;
    }

    
}
