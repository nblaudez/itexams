<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class TakenExercice
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $exerciceId;



    /**
     *  @MongoDB\Field(type="string")
     */
    protected $exerciceName;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userType;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $userId;


    /**
     *  @MongoDB\Field(type="string")
     */
    protected $guestId;

    /**
     * @MongoDB\EmbedMany(targetDocument="TakenExerciceQuestion")
     */
    protected $questions;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $result;

    /**
     * @MongoDB\Date
     */
    protected $date;

    /**
     *  @MongoDB\Field(type="string")
     */

    protected $language;


    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set guestId
     *
     * @param String $guestId
     * @return self
     */
    public function setGuestId($guestId)
    {
        $this->guestId = $guestId;
        return $this;
    }

    /**
     * Get guestId
     *
     * @return string $guestId
     */
    public function getGuestId()
    {
        return $this->guestId;
    }

    /**
     * Set testId
     *
     * @param String $testId
     * @return self
     */
    public function setExerciceId($exerciceId)
    {
        $this->exerciceId = $exerciceId;
        return $this;
    }

    /**
     * Get testId
     *
     * @return string $testId
     */
    public function getExerciceId()
    {
        return $this->exerciceId;
    }

    /**
     * Set exerciceName
     *
     * @param string $testexerciceNameName
     * @return self
     */
    public function setExerciceName($exerciceName)
    {
        $this->exerciceName = $exerciceName;
        return $this;
    }

    /**
     * Get exerciceName
     *
     * @return string $exerciceName
     */
    public function getExerciceName()
    {
        return $this->exerciceName;
    }

    /**
     * Set userType
     *
     * @param string $userType
     * @return self
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;
        return $this;
    }

    /**
     * Get userType
     *
     * @return string $userType
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set userId
     *
     * @param string $userId
     * @return self
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * Get userId
     *
     * @return string $userId
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Add question
     *
     */
    public function addQuestion($question)
    {
        $this->questions[]=$question;
    }



    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection $questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }

    /**
     * Set result
     *
     * @param int $result
     * @return self
     */
    public function setResult($result)
    {
        $this->result = $result;
        return $this;
    }

    /**
     * Get result
     *
     * @return int $result
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set date
     *
     * @param date $date
     * @return self
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * Get date
     *
     * @return date $date
     */
    public function getDate()
    {
        return $this->date;
    }

   

    /**
     * @return mixed
     */
    public function getSitePercentage()
    {
        return $this->sitePercentage;
    }

    /**
     * @param mixed $sitePercentage
     */
    public function setSitePercentage($sitePercentage)
    {
        $this->sitePercentage = $sitePercentage;
    }

    /**
     * Remove question
     *
     * @param CoreBundle\Document\TakenExerciceQuestion $question
     */
    public function removeQuestion(\CoreBundle\Document\TakenExerciceQuestion $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }
}
