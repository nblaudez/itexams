<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;


/**
 * @MongoDB\Document
 */
class Question
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $testId;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $question;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $explanation;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $activated;

    /**
     * @MongoDB\Collection
     */
    protected $answers;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $goodAnswered;

    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $badAnswered;

    /**
     * @MongoDB\EmbedMany(targetDocument="Tag")
     */
    protected $tags;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $categoryId;



    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get testId
     *
     * @return id $testId
     */
    public function getTestId()
    {
        return $this->testId;
    }

    /**
     * Get testId
     *
     * @return id $testId
     */
    public function setTestId($testId)
    {
        $this->testId = $testId;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return self
     */
    public function setQuestion($question)
    {
        $this->question = $question;
        return $this;
    }

    /**
     * Get question
     *
     * @return string $question
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set explanation
     *
     * @param string $explanation
     * @return self
     */
    public function setExplanation($explanation)
    {
        $this->explanation = $explanation;
        return $this;
    }

    /**
     * Get explanation
     *
     * @return string $explanation
     */
    public function getExplanation()
    {
        return $this->explanation;
    }

    /**
     * Set activated
     *
     * @param bool $activated
     * @return self
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
        return $this;
    }

    /**
     * Get activated
     *
     * @return bool $activated
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set answers
     *
     * @param collection $answers
     * @return self
     */
    public function setAnswers($answers)
    {
        $this->answers = $answers;
        return $this;
    }

    /**
     * Get answers
     *
     * @return collection $answers
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set goodAnswered
     *
     * @param int $goodAnswered
     * @return self
     */
    public function setGoodAnswered($goodAnswered)
    {
        $this->goodAnswered = $goodAnswered;
        return $this;
    }

    /**
     * Get goodAnswered
     *
     * @return int $goodAnswered
     */
    public function getGoodAnswered()
    {
        return $this->goodAnswered;
    }

    /**
     * Set badAnswered
     *
     * @param int $badAnswered
     * @return self
     */
    public function setBadAnswered($badAnswered)
    {
        $this->badAnswered = $badAnswered;
        return $this;
    }

    /**
     * Get badAnswered
     *
     * @return int $badAnswered
     */
    public function getBadAnswered()
    {
        return $this->badAnswered;
    }

    /**
     * Add Tag
     *
     * @param CoreBundle\Document\Tag $Tag
     */
    public function addTag(\CoreBundle\Document\Tag $Tag)
    {
        $this->tags[] = $Tag;
    }

    /**
     * Remove Tag
     *
     * @param CoreBundle\Document\Tag $Tag
     */
    public function removeTag(\CoreBundle\Document\Tag $Tag)
    {
        $this->tags->removeElement($Tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection $tags
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set tags
     *
     * @param Array $tags
     * @return self
     */
    public function setTags($tags)
    {
        return $this->tags = $tags;
    }

    /**
     * @return self
     */
    public function cleanTags()
    {
        $this->tags = null;
        return $this;
    }


    /**
     * Set categoryId
     *
     * @param $categoryId
     * @return self
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * Get categoryId
     *
     * @return string $categoryId
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }
    public function __construct()
    {
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
}
