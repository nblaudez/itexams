<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Response
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $goodAnswer;

    /**
     *  @MongoDB\Field(type="string")
     */
    protected $response;

    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $activated;

    /**
     * @MongoDB\Collection
     */
    protected $categories;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set goodAnswer
     *
     * @param bool $goodAnswer
     * @return self
     */
    public function setGoodAnswer($goodAnswer)
    {
        $this->goodAnswer = $goodAnswer;
        return $this;
    }

    /**
     * Get goodAnswer
     *
     * @return bool $goodAnswer
     */
    public function getGoodAnswer()
    {
        return $this->goodAnswer;
    }

    /**
     * Set response
     *
     * @param string $response
     * @return self
     */
    public function setResponse($response)
    {
        $this->response = $response;
        return $this;
    }

    /**
     * Get response
     *
     * @return string $response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set activated
     *
     * @param bool $activated
     * @return self
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
        return $this;
    }

    /**
     * Get activated
     *
     * @return bool $activated
     */
    public function getActivated()
    {
        return $this->activated;
    }

    /**
     * Set categories
     *
     * @param collection $categories
     * @return self
     */
    public function setCategories($categories)
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * Get categories
     *
     * @return collection $categories
     */
    public function getCategories()
    {
        return $this->categories;
    }
}
