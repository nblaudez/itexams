<?php
namespace CoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\EmbeddedDocument
 */
class Category
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;


    /**
     * @MongoDB\Field(type="string")
     */
    protected $name;


    /**
     *  @MongoDB\Field(type="integer")
     */
    protected $numberOfQuestions;


    /**
     *  @MongoDB\Field(type="bool")
     */
    protected $activated;




    public function __construct()
    {
        
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set numberOfQuestions
     *
     * @param int $numberOfQuestions
     * @return self
     */
    public function setNumberOfQuestions($numberOfQuestions)
    {
        $this->numberOfQuestions = $numberOfQuestions;
        return $this;
    }

    /**
     * Get numberOfQuestions
     *
     * @return int $numberOfQuestions
     */
    public function getNumberOfQuestions()
    {
        return $this->numberOfQuestions;
    }

    /**
     * Set activated
     *
     * @param bool $activated
     * @return self
     */
    public function setActivated($activated)
    {
        $this->activated = $activated;
        return $this;
    }

    /**
     * Get activated
     *
     * @return bool $activated
     */
    public function getActivated()
    {
        return $this->activated;
    }

    public function __toString() {
        return $this->getId();
    }
}
