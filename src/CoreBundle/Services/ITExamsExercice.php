<?php
namespace CoreBundle\Services;



use CoreBundle\Document\User;

class ITExamsExercice
{

    public function __construct($doctrine, $userManager, $mailer)
    {

        $this->_doctrine = $doctrine;
        $this->_userManager = $userManager;
        $this->_mailer = $mailer;

    }




    public function retrieveUserExercice($userId) {
        $results=[];
        foreach($this->_doctrine->getRepository("CoreBundle:TakenExercice")->findBy(['$or'=>[["userId"=>$userId],["guestId"=>$userId]]],array('date' => 'DESC')) as $takenExercice) {
            $results[]=["result"=>$takenExercice->getResult(),
                        "exercice"=>$this->_doctrine->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId()),
                        "name"=>$takenExercice->getExerciceName(),
                        "questions"=>$takenExercice->getQuestions(),
                        "date"=>$takenExercice->getDate(),
                        "takenExercice"=>$takenExercice,
                        "id"=>$takenExercice->getId()
            ];
        }

        return $results;
    }


}