<?php
namespace CoreBundle\Services;


use CoreBundle\Document\Withdrawal;
use Symfony\Component\Validator\Constraints\Bic;
use Symfony\Component\Validator\Constraints\Iban;


class ITExamsAffiliation
{
    public function __construct($mongodb, $mailler, $validator)
    {
        $this->_mailer = $mailler;
        $this->mongodb = $mongodb;
        $this->validator = $validator;
    }

    public function invite($user,$email) {


        $affiliateId = $user->getId();
        $firstname = $user->getFirstname();
        $lastname = $user->getLastname();

        $messageBody=<<<EOF
Hello ,<br /><br />
I invite you to visit <a href="">http://www.it-exams.tech/?AFFILIATE_ID=$affiliateId</a>.<br />
This web site permits to evaluate candidates  about theirs skills.<br /> 
<br />
Best regards,<br />
<br />
$firstname $lastname
EOF;

        $message = \Swift_Message::newInstance()
            ->setSubject("Hi, i suggest you to visit a website")
            ->setFrom($user->getEmail())
            ->setTo($email)
            ->setBody(
                $messageBody,
                'text/html'
            );
        return $this->_mailer->send($message);

    }

    public function setBankInfo($userId,$iban,$swift) {

        $errorsList=[];

        $ibanConstraint = new Iban();
        $ibanConstraint->message = 'Invalid IBAN';
        $ibanError= $this->validator->validateValue($iban, $ibanConstraint);

        $swiftConstraint = new Bic();
        $swiftConstraint->message = 'Invalid BIC';
        $swiftError = $this->validator->validateValue($swift, $swiftConstraint);

        if(count($swiftError)>0)
            $errorsList[]=$swiftError;

        if(count($ibanError)>0)
            $errorsList[]=$ibanError;

        if(count($errorsList))
            return $errorsList;

        $user = $this->mongodb->getRepository("CoreBundle:User")->find($userId);

        $user->setIban($iban);
        $user->setSwift($swift);

        $em = $this->mongodb->getManager();

        $em->persist($user);
        $em->flush();

        return true;

    }

    public function createWidthdrawal($user,$amount) {
        $withdrawal = new Withdrawal();
        $withdrawal->setUserId($user->getId());
        $withdrawal->setAmount($amount);
        $withdrawal->setDate(time());
        $withdrawal->setStatus(0);

        $em = $this->mongodb->getManager();

        $em->persist($withdrawal);
        $em->flush();
    }
}