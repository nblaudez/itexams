<?php
namespace CoreBundle\Services;


use CoreBundle\Document\User;

class ITExamsUser
{

    public function __construct($doctrine, $userManager, $mailer)
    {

        $this->_doctrine = $doctrine;
        $this->_userManager = $userManager;
        $this->_mailer = $mailer;

    }

    public function HasAPlan($userId)
    {
        $credits = $this->_doctrine->getRepository("CoreBundle:Credit")->findByUserId($userId);
        if (!isset($credits[0])) {
            return false;
        } else {
            $credits = $credits[0];
        }
        if ($credits->getCredits() == -1 || $credits->getCredits() >= 1) {
            if ($credits->getCredits() != -1) {
                $credits->setCredits($credits->getCredits() - 1);
            }
            return true;
        }

        return false;

    }


    public function userExists($email)
    {
        $user = $this->_doctrine->getRepository("CoreBundle:User")->findByEmailCanonical(strtolower($email));
        if (count($user) > 0)
            return true;
        else
            return false;
    }

    public function createUser($data)
    {

        $password = $this->generatePassword();

        $user = $this->_userManager->createUser();
        $user->setEmail($data['email']);
        $user->setUsername($data['email']);
        $user->setFirstname($data['firstname']);
        $user->setLastname($data['lastname']);
        $user->setPlainPassword($password);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $this->_userManager->updateUser($user);

        $firstname = $data["firstname"];
        $lastname = $data["lastname"];
        $email = $data["email"];

        $messageBody = <<<EOF
Hello $firstname $lastname,<br /><br />
A account was been created for you on IT-EXAMS.TECH.<br />
Your login is : $email<br />
Password : $password<br />
IT-Exams.tech team<br />
EOF;

        $message = \Swift_Message::newInstance()
            ->setSubject("Your account on IT-EXAMS")
            ->setFrom('noreply@it-exams.tech')
            ->setTo($email)
            ->setBody(
                $messageBody,
                'text/html'
            );
        $this->_mailer->send($message);

        return ['user' => $user, 'password' => $password];
    }

    public function inviteUser($user, $takenTest, $company)
    {

        $firstname = $user->getFirstname();
        $company_name = $company->getCompanyName();
        $lastname = $user->getLastName();
        $testId = $takenTest->getId();
        $testName = $this->_doctrine->getRepository("CoreBundle:Test")->find($takenTest->getTestId())->getName();

        $messageBody = <<<EOF
<html>        
Hello $firstname $lastname,<br /><br />
$company_name has create a test you must take for evaluation.<br />
Click on the link bellow to start the test $testName.<br />
<br />
<a href='http://www.it-exams.tech/guest/start/$testId'>Start the test</a><br />
<br />
$company_name
</html>
EOF;

        $customisation = $this->_doctrine->getRepository("CoreBundle:Customisation")->findByUserId($company->getId());
        if($customisation) {
            $customisation = $customisation[0];
        } 

        if(count($customisation)> 0 && $customisation->getContactEmail() != null ) {
            $from=$customisation->getContactEmail();
        } else {
            $from="no-reply@it-exams.tech";
        }

        $message = \Swift_Message::newInstance()
            ->setSubject("Take a test on IT-Exams for $company_name")
            ->setFrom($from)
            ->setTo($user->getEmail())
            ->setBody(
                $messageBody,
                "text/html"
            );
        $this->_mailer->send($message);
    }

    public function inviteExerciceUser($user, $takenExercice, $company)
    {

        $firstname = $user->getFirstname();
        $company_name = $company->getCompanyName();
        $lastname = $user->getLastName();
        $exerciceId = $takenExercice->getId();
        $exerciceName = $this->_doctrine->getRepository("CoreBundle:Exercice")->find($takenExercice->getExerciceId())->getName();

        $messageBody = <<<EOF
<html>
Hello $firstname $lastname,<br /><br />
$company_name has create a exercice you must take for evaluation.<br />
Click on the link bellow to start the test $exerciceName.<br />
<br />
<a href='http://www.it-exams.tech/guest/start/exercice/$exerciceId'>Start the test</a><br />
<br />
$company_name
</html>
EOF;

        $customisation = $this->_doctrine->getRepository("CoreBundle:Customisation")->findByUserId($company->getId())[0];

        if(count($customisation)> 0 && $customisation->getContactEmail() != null ) {
            $from=$customisation->getContactEmail();
        } else {
            $from="no-reply@it-exams.tech";
        }

        $message = \Swift_Message::newInstance()
            ->setSubject("Take an exercice for $company_name")
            ->setFrom($from)
            ->setTo($user->getEmail())
            ->setBody(
                $messageBody,
                'text/plain'
            );
        $this->_mailer->send($message);
    }


    public function generatePassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass .= $alphabet[$n];
        }
        return $pass;
    }

    public function retrieveGraphData($companyId,$userId)
    {
        $tests = $this->_doctrine->getRepository("CoreBundle:Test")->findBy(array(
            '$or' => array(
                array('userId' => $companyId),
                array('userId' => null),
            )
        ));
        $results = [];
        foreach ($tests as $i => $test) {
            $results[$i] = ["result" => 0, "nbQuestions" => 0, "testId" => $test->getId()];
        }


        if($companyId == $userId) {
            $takenTests = $this->_doctrine->getRepository("CoreBundle:TakenTest")->findBy(array(
                '$and' => array(
                    array('userId' => $companyId),
                    array('guestId' => null),
                )
            ));
        } else {
            $takenTests = $this->_doctrine->getRepository("CoreBundle:TakenTest")->findBy(array(
                '$and' => array(
                    array('userId' => $companyId),
                    array('guestId' => $userId),
                )
            ));
        }
        foreach ($results as $j => $result) {
            foreach ($takenTests as $i => $takentest) {
                if ($results[$j]["testId"] == $takentest->getTestId() ) {
                    $results[$j]["result"] += $takentest->getResult();
                    $results[$j]["userId"] = $takentest->getUserId();
                    $results[$j]["guestId"] = $takentest->getGuestId();
                    $results[$j]["nbQuestions"] += count($takentest->getQuestions());
                }

            }
        }

        return $results;
    }

    public function retrieveAllGraphData($userId)
    {
        $takenTests = $this->_doctrine->getRepository("CoreBundle:TakenTest")->findByUserId($userId);
        $results = $users = [];
        foreach ($takenTests as $i => $takenTest) {
            if ($takenTest->getGuestId() != null) {
                $user = $this->_doctrine->getRepository("CoreBundle:User")->find($takenTest->getGuestId());

            } else {
                $user = $this->_doctrine->getRepository("CoreBundle:User")->find($takenTest->getUserId());

            }
            if (!in_array($user->getId(), $users)) {
                $results["users"][$i]["userId"] = $user->getId();
                $results["users"][$i]["lastname"] = $user->getLastname();
                $results["users"][$i]["firstname"] = $user->getFirstname();
                if ($takenTest->getGuestId() != null) {
                    $results["users"][$i]["results"] = $this->retrieveGraphData($takenTest->getUserId(),$takenTest->getGuestId());
                } else {
                    $results["users"][$i]["results"] = $this->retrieveGraphData($takenTest->getUserId(),$takenTest->getUserId());
                }
                $users[] = $user->getId();

            }

        }
        $results["tests"] = $this->_doctrine->getRepository("CoreBundle:Test")->findAll();
        return $results;
    }


    public
    function retrieveCompanyUser($userId)
    {
        $users = [];
        $takenTest = $this->_doctrine->getRepository("CoreBundle:TakenTest")->findByUserId($userId);
        foreach ($takenTest as $TTest) {
            if ($TTest->getGuestId() != null) {
                $users[] = $this->_doctrine->getRepository("CoreBundle:User")->find($TTest->getGuestId());
            } else {
                $users[] = $this->_doctrine->getRepository("CoreBundle:User")->find($TTest->getUserId());
            }
        }

        return array_unique($users);
    }


    public function checkCredit($userId)
    {
        $credit = $this->_doctrine->getRepository("CoreBundle:Credit")->findByUserId($userId);
        if ($credit) {
            $credit = $credit[0];
        } else {
            header("Location: /dashboard/credits/recharge");
            die;
        }
        if($credit->getCredits()==0) {
            header("Location: /dashboard/credits/recharge");
            die;
        }
    }

    public function decreaseCredit($userId) {
        $credit = $this->_doctrine->getRepository("CoreBundle:Credit")->findByUserId($userId)[0];
        $credit->setCredits($credit->getCredits()-1);
        $em = $this->_doctrine->getManager();
        $em->persist($credit);
        $em->flush();
    }
}