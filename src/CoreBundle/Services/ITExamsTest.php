<?php
namespace CoreBundle\Services;



use CoreBundle\Document\User;
use Symfony\Component\HttpFoundation\Session\Session;

class ITExamsTest
{

    public function __construct($doctrine, $userManager, $mailer, $templating)
    {

        $this->_doctrine = $doctrine;
        $this->_userManager = $userManager;
        $this->_mailer = $mailer;
        $this->_templating = $templating;

    }

    public function retrieveQuestion($testId)
    {
        $test = $this->_doctrine->getRepository("CoreBundle:Test")->find($testId);

        $returnedQuestion = [];
        foreach ($test->getCategories() as $category) {
            $questions=$this->_doctrine->getRepository("CoreBundle:Question")->findByCategoryId($category->getId());
            shuffle($questions);
            $questions = array_slice($questions, 0, $category->getNumberOfquestions());
            $returnedQuestion = array_merge($returnedQuestion,$questions);
        }
        foreach($returnedQuestion as $question) {
            $result[]=$question->getId();
        }
        return $result;
    }

    public function saveResults($testTakenId)
    {

        $takenTest = $this->_doctrine->getRepository("CoreBundle:TakenTest")->find($testTakenId);
        $test = $this->_doctrine->getRepository("CoreBundle:Test")->find($takenTest->getTestId());

        $goodResponses = $responses = [];

        foreach ($takenTest->getQuestions() as $question) {
            $dQuestion = $question->getQuestion();
            foreach ($dQuestion->getAnswers() as $id => $answers) {
                $goodResponses[$dQuestion->getId()][$id] = $answers['goodResponse'];
            }
            if (count($question->getAnswers()) > 0) {
                foreach ($question->getAnswers() as $key => $answer) {
                    ($answer == "on") ? $answer = "Yes" : $answer = "No";
                    $userResponses[$dQuestion->getId()][$key] = $answer;
                }
            }
        }


        $result = 0;
        $categories = [];
        foreach ($goodResponses as $questionId => $responses) {
            $tmp_result = 1;
            foreach ($responses as $key => $response) {
                if ((!isset($userResponses[$questionId][$key]) && $response == "Yes") || (isset($userResponses[$questionId][$key]) && $userResponses[$questionId][$key] != $response)) {
                    $tmp_result = 0;
                }
            }
            $question = $this->_doctrine->getRepository("CoreBundle:Question")->find($questionId);
            if (!isset($categories[$question->getCategoryId()])) {
                $categories[$question->getCategoryId()] = ['_id' => $question->getCategoryId(), 'result' => 0];
            }
            $categories[$question->getCategoryId()]['result'] += $tmp_result;
            $result += $tmp_result;
        };

        $totalPercent = [];
        foreach ($categories as $key => $categoryData) {
            foreach ($test->getCategories() as $categoryTest) {
                if ($categoryTest->getId() == $key) {
                    $percent = floor($categoryData["result"] / $categoryTest->getNumberOfQuestions() * 100);
                    $categories[$key]["percent"] = $percent;
                    $totalPercent[] = $percent;
                }
            }
        }
        $totalPercent = array_sum($totalPercent) / count($totalPercent);
        $totalPercent = ceil($totalPercent / 10) * 10;

        $keyPercent = (($totalPercent / 10) - 1 >= 0) ? ($totalPercent / 10) - 1 : 0;
        $percentArray = $test->getResults();

        $percentArray[$keyPercent]["result"] = $percentArray[$keyPercent]["result"] + 1;
        $test->setResults($percentArray);
        $takenNumber = (int)$test->getTakenNumber() + 1;
        $test->setTakenNumber($takenNumber);



        $session = new Session();
        $dateStart = $session->get("dateStart");
        $duration = time() - $dateStart;
        $takenTest->setDuration($duration);

        foreach ($categories as $key => $cat) {
            $dbCat = $this->_doctrine->getRepository("CoreBundle:Category")->find($cat["_id"]);
            $categories[$key]["name"] = $dbCat->getName();
        }
        $takenTest->setCategoriesResult($categories);
        $takenTest->setResult($result);


        $em = $this->_doctrine->getManager();

        $em->persist($takenTest);
        $em->persist($test);

        $em->flush($takenTest);
        $em->flush($test);
        

        /* Calculate rank */
        $j=1;
        for ($i = $test->getNumberOfQuestions(); $i >= 0; $i--) {
            $entities = $em->createQueryBuilder('CoreBundle:TakenTest')
                ->field('result')->equals($i)
                ->field('testId')->equals($test->getId())
                ->sort('duration', 'ASC')
                ->getQuery()
                ->execute();


            foreach ($entities as $entity) {
                $entity->setRang($j);
                if($j==1)
                    $place=0;
                else
                    $place=$j;
                $entity->setSitePercentage(ceil(($test->getTakenNumber()-$place)/$test->getTakenNumber()*100));
                $em->persist($entity);
                $em->flush($entity);
                $j++;
            }
        }

        /**
         * Send results mail
         */
        $results = $this->retrieveDetailedResults($testTakenId);


        $customisation = $this->_doctrine->getRepository("CoreBundle:Customisation")->findByUserId($takenTest->getUserId());
        if($customisation) {
            $customisation = $customisation[0];
        }

        $user=$this->_doctrine->getRepository("CoreBundle:User")->find($takenTest->getUserId());
        $emails[]=$user->getEmail();

        if(count($customisation)> 0 && $customisation->getContactEmail() != null ) {
            $from=$customisation->getContactEmail();
        } else {
            $from="no-reply@it-exams.tech";
        }

        if($takenTest->getGuestId()!=null) {
            $guestUser=$this->_doctrine->getRepository("CoreBundle:User")->find($takenTest->getGuestId());
            $emails[]=$guestUser->getEmail();
        }

        foreach($emails as $email) {
            $message = \Swift_Message::newInstance()
                ->setSubject("IT-EXAMS : Result of test ".$takenTest->getTestName())
                ->setFrom(($email==$user->getEmail()) ? "no-reply@it-exams.tech" : $from)
                ->setTo($email)
                ->setBody($this->_templating->render(
                    'CoreBundle:Test:resultDetails.html.twig',
                    array('noLayout'=>true,'results' => $results,'test'=>$test,"categories"=>$results['categories'])
                ),
                    'text/html'
                );
            $this->_mailer->send($message);
        }
        return  true;

    }

    public function retrieveUserTests($userId) {
        $results=[];
        foreach($this->_doctrine->getRepository("CoreBundle:TakenTest")->findBy(['$or'=>[["userId"=>$userId],["guestId"=>$userId]]],array('date' => 'DESC')) as $takenTest) {
            $results[]=["result"=>$takenTest->getResult(),
                        "test"=>$this->_doctrine->getRepository("CoreBundle:Test")->find($takenTest->getTestId()),
                        "testName"=>$takenTest->getTestName(),
                        "questions"=>$takenTest->getQuestions(),
                        "date"=>$takenTest->getDate(),
                        "id"=>$takenTest->getId()
            ];
        }

        return $results;
    }

    public function retrieveDetailedResults($takenId) {

        $takenTest = $this->_doctrine->getRepository("CoreBundle:TakenTest")->find($takenId);
        $test = $this->_doctrine->getRepository("CoreBundle:Test")->find($takenTest->getTestId());
        $totalPercent = 0;
        foreach($takenTest->getCategoriesResult() as $category) {
            $testData = $this->_doctrine->getRepository("CoreBundle:Test")->find($takenTest->getTestId());
            foreach($testData->getCategories() as $cat) {
                if($cat->getId()==$category["_id"]) {
                    $categories[]=["result"=>$category["result"],"percent"=>$category["percent"],"name"=>$cat->getName()];
                }
            }
        }

        return ["takenTest"=>$takenTest,"categories"=>$categories,"test"=>$test];
    }



    public function retrieveAllUserResults($userId) {
        $takenTests = array_merge(
                            $this->_doctrine->getRepository("CoreBundle:TakenTest")->findByUserId($userId),
                            $this->_doctrine->getRepository("CoreBundle:TakenTest")->findByGuestId($userId)
                      );
        $results=[];
        foreach($takenTests as $takenTest) {
            if(($takenTest->getGuestId()!=null && $takenTest->getGuestId()==$userId) || $takenTest->getGuestId()==null) {
                $results[]=$this->retrieveDetailedResults($takenTest->getId());
            }
        }

        return $results;
    }
}