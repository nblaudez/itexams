<?php

namespace CoreBundle\Command;

use CoreBundle\Document\Category;
use CoreBundle\Document\Question;
use CoreBundle\Document\Test;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class SetSectionIDCommand extends ContainerAwareCommand
{
    protected $container;

    protected function configure()
    {
        $this
            ->setName('itexams:setsectionid')
            ->setDescription('Set tests session id');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get("doctrine_mongodb")->getManager();


        /** Set All test to zero */
            
        $technos = $em->getRepository("CoreBundle:Sector")->findAll();
        foreach($technos as $techno) {
            $techno->setSectionId("58e12e3b1bd4b1471fdbe9a7");
            $em->persist($techno);
            $em->flush($techno);
        }

    }


}