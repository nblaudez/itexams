<?php

namespace CoreBundle\Command;

use CoreBundle\Document\Category;
use CoreBundle\Document\Question;
use CoreBundle\Document\Test;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class ImptestsCommand extends ContainerAwareCommand
{
    protected $container;

    protected function configure()
    {
        $this
            ->setName('itexams:imptests')
            ->setDescription('Import file system tests');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get("doctrine_mongodb")->getManager();

        $file = "/home/sites/com/certification-trainings/questions/php54/test.yml";
        $yaml = new Parser();
        $config = $yaml->parse(file_get_contents($file));

        for ($j = 1; $j <= 14; $j++) {

            $test = new Test();


            $technology = $em->getRepository("CoreBundle:Sector")->findBySlug("php54")[0];

            $test->setTechnologyId($technology->getId());
            $test->setName(trim(str_ireplace('fundamentals','',$config["name"])) . " test ".strtoupper(chr(64+$j)));
            $test->setSlug($config["slug"]);
            if (isset($config["description"])) {
                $test->setDescription($config["description"]);
            }
            if (isset($config["number_questions"])) {
                $test->setNumberQuestions($config["number_questions"]);
            }
            if (isset($config["duration"])) {
                $test->setTime($config["duration"]);
            }


            if (file_exists(dirname($file) . "/instruction.txt")) {
                $test->setInstructions(file_get_contents(dirname($file) . "/instruction.txt"));
            }
            $test->setCore(true);
            $test->setNameSlugued($this->sluggify($config["name"] . " - $j"));
            $test->setActivated(true);
            $results=[];
            for($k=1;$k<=10;$k++) {
                $results[]=["value"=>$k*10,"result"=>"0"];
            }
            $test->setResults($results);

            if (isset($config["categories"])) {
                foreach ($config["categories"] as $cat) {
                    $category = new Category();
                    $category->setActivated(true);
                    $category->setName($cat["name"]);
                    $category->setActivated(1);
                    if (isset($cat["number_questions"])) {
                        $category->setNumberOfQuestions($cat["number_questions"]);
                    }
                    if (isset($cat["dir"])) {
                        $questions = glob("/home/sites/com/certification-trainings/questions/" . $config["slug"] . "/" . $cat["dir"] . "/*");
                    }

                    $test->addCategory($category);


                    $em->persist($test);
                    $em->flush();


                    $i = 0;
                    if (isset($cat["dir"])) {
                        foreach ($questions as $dir) {
                            if (is_dir($dir)) {
                                if ($i < $cat["number_questions"]) {
                                    $question = file_get_contents($dir . "/question.txt");
                                    $answersQuestion = glob($dir . "/answers/*.txt");
                                    $mQuestion = new Question();
                                    $answers = [];
                                    foreach ($answersQuestion as $answerFile) {
                                        if (strstr($answerFile, "true")) {
                                            $goodResponse = true;
                                        } else {
                                            $goodResponse = false;
                                        }

                                        $answerContent = utf8_encode(file_get_contents($answerFile));

                                        $answers[] = ["goodResponse" => $goodResponse, "response" => $answerContent];

                                    }


                                    $mQuestion->setAnswers($answers);
                                    $mQuestion->setTestId($test->getId());
                                    $mQuestion->setQuestion(utf8_encode($question));
                                    $mQuestion->setCategoryId($category->getId());


                                    $em->persist($mQuestion);
                                    $em->flush();
                                    $this->deleteDir($dir);

                                    $i++;
                                }
                            }
                        }
                    }
                    
                }
            }

        }

        $em->persist($technology);
        $em->flush();
    }


    public static function deleteDir($dirPath)
    {
        if (!is_dir($dirPath)) {
            throw new InvalidArgumentException("$dirPath must be a directory");
        }
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                self::deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }


    public function sluggify($str)
    {
        # special accents
        $a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?');
        $b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $slug = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), str_replace($a, $b, $str)));
        $slug = str_replace(array("\r", "\n"), '', $slug);
        return $slug;
    }

}