<?php

namespace CoreBundle\Command;

use CoreBundle\Document\Category;
use CoreBundle\Document\Question;
use CoreBundle\Document\Test;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Yaml\Parser;
use Symfony\Component\Yaml\Yaml;

class CleanDbCommand extends ContainerAwareCommand
{
    protected $container;

    protected function configure()
    {
        $this
            ->setName('itexams:cleanDb')
            ->setDescription('Clean questions collection');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->container = $this->getApplication()->getKernel()->getContainer();
        $em = $this->container->get("doctrine_mongodb")->getManager();

         /*
        $questions = $em->getRepository("CoreBundle:Question")->findAll();
        foreach($questions as $question) {
            $test = $em->getRepository("CoreBundle:Test")->find($question->getTestId());
            if($test == null) {
                $em->remove($question);
                $em->flush();
            }

        }
         */


        /**
         * Clean TakenTest
         */
        /*
        $takenTests = $em->getRepository("CoreBundle:TakenTest")->findAll();
        foreach($takenTests as $takenTest) {
            $test = $em->getRepository("CoreBundle:Test")->find($takenTest->getTestId());
            if($test == null) {
                $em->remove($takenTest);
                $em->flush($takenTest);
            }

        }
        */

        /**
         * Clean PHP Test
         */
    /*
        $tests = $em->getRepository("CoreBundle:Test")->findByTechnologyId("57419976c46988c2608b4578");
        foreach($tests as $test) {
            $em->remove($test);
          $em->flush();
        }
*/

        /** Set setactorID */
            
        $tests = $em->getRepository("CoreBundle:Test")->findAll();
        foreach($tests as $test) {
            $test->setsectorId($test->getTechnologyId());
            $em->persist($test);
            $em->flush($test);
        }

        $output->writeln("## OK");

    }


}