echo "starting backup"
echo $BAK
/usr/bin/mongodump  --db=itexams --out=dump/
echo "backup created"
/usr/bin/git add .
/usr/bin/git commit -m "backup from `date +'%Y_%m_%d'`"
echo "committed"
/usr/bin/git push -u origin master
echo "pushed backup. done"
