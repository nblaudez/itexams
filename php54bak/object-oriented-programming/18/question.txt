What is the output of the following code ?

<?php
class parentclass {
    	
    public function sayHello() {
        echo "Hello world";
    }
}

class classname extends parentclass {
    public function __construct() {
        parent::sayHello();
    }
	
    public function sayHello() {
        echo "Hello darling";
    }
}

$obj=new classname();
?>