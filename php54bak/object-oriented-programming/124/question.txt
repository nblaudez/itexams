What is the output of the following code ?

<?php

class class1{
    public function getClass() {
	return get_class();
    }
}
class class2 extends class1{
}
$obj=new class2();
echo $obj->getClass();
?>
