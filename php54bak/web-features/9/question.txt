Output_Buffering configuration directive is set to Off.

What does the following code print ?

<?php
echo "Hello ";
setcookie('A_Cookie','world');
echo $_COOKIE['A_Cookie'];
?> 