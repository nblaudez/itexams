What does the following code print ?

<?php
$xml=<<<'EOF'
<compagny>
    <employe id="15">Marcel CHAMPION</employe>
    <employe id="16">Bernard MARTIN</employe>
    <employe id="17">Rene FOSCH</employe>
</compagny>
EOF;

$simpleXML=new SimpleXMLElement($xml);

echo $simpleXML->employe[1]['id'];
?>