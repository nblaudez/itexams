What does the following code print ?

<?php
$xml=<<<'EOF'
<books>
    <book id="1"><title>PHP for experts</title><pages>865</pages></book>
    <book id="2"><title>PHP for programmers</title><pages>652</pages> </book>
    <book id="3"><title>PHP for dummies</title><pages>450</pages> </book>
</books>
EOF;

$simpleXML=simplexml_load_string($xml);
$result=$simpleXML->xpath('/books/book[3]/pages');

foreach($result as $entry)
    echo $entry." ";
?>