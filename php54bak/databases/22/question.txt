What does the following query on the following tables return?


SELECT P.NAME, E.EMAIL
FROM persons P
LEFT OUTER JOIN emails E 
            ON E.PERS_ID = P.PERS_ID


Table "persons"


!#table#!
!#tr#!!#td#!  PERS_ID !#/td#!!#td#! NAME      !#/td#!!#td#! FIRSTNAME !#/td#!!#/tr#!
!#tr#!!#td#!  1       !#/td#!!#td#! Mcgregor  !#/td#!!#td#! Henry  !#/td#!!#/tr#!
!#tr#!!#td#!  2       !#/td#!!#td#! Stoward   !#/td#!!#td#! Peter  !#/td#!!#/tr#!
!#/table#!


Table "emails"

!#table#!
!#tr#!!#td#!  ID !#/td#!!#td#! PERS_ID      !#/td#!!#td#! EMAIL !#/td#!!#/tr#!
!#tr#!!#td#!  1    !#/td#!!#td#! 1  !#/td#!!#td#! henry@e-commerce.com  !#/td#!!#/tr#!
!#tr#!!#td#!  2    !#/td#!!#td#! 3  !#/td#!!#td#! john@business.it   !#/td#!!#/tr#!
!#/table#!