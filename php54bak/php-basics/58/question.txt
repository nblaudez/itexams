What does the following code print ?

<?php

const MY_FIRST_CONST = 'hello';
define('MY_SECOND_CONST', 'world');

echo MY_FIRST_CONST."  ".MY_SECOND_CONST;
?>