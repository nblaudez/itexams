What is the output of the following code ?

<?php
namespace FooBarBazDoe;

function test() {
    return "Hello, world";
}


namespace OtherNamespacename;
use FooBarBazDoe;

echo test();
?>