-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: certificationtrainings
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb8u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `test_slug` varchar(60) NOT NULL,
  `results` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,5,'Nicolas Blaudez','2016-03-20 17:58:10','php54','1'),(2,5,'Nicolas Blaudez','2016-03-20 17:58:26','php54','1'),(3,5,'Nicolas Blaudez','2016-03-20 17:58:41','php54','0'),(4,5,'Nicolas Blaudez','2016-03-20 17:59:29','php54','0'),(5,5,'Nicolas Blaudez','2016-03-20 17:59:57','php54','1'),(6,5,'Nicolas Blaudez','2016-03-20 18:00:13','php54','0');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test_question`
--

DROP TABLE IF EXISTS `test_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `test_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `review` tinyint(1) NOT NULL,
  `correct` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test_question`
--

LOCK TABLES `test_question` WRITE;
/*!40000 ALTER TABLE `test_question` DISABLE KEYS */;
INSERT INTO `test_question` VALUES (1,91,25,0,0),(2,91,83,0,0),(3,91,85,0,0),(4,91,70,0,0),(5,91,74,0,0),(6,91,42,0,0),(7,91,33,0,0),(8,91,17,0,0),(9,91,74,0,0),(10,91,11,0,0),(11,91,42,0,0),(12,91,43,0,0),(13,91,49,0,0),(14,91,51,0,0),(15,91,10,0,0),(16,91,23,0,0),(17,91,10,0,0),(18,91,33,0,0),(19,91,59,0,0),(20,91,43,0,0),(21,91,87,0,0),(22,91,49,0,0),(23,91,29,0,0),(24,91,90,0,0),(25,91,32,0,0),(26,91,78,0,0),(27,91,32,0,0),(28,91,10,0,0),(29,91,35,0,0),(30,91,52,0,0),(31,91,21,0,0),(32,91,53,0,0),(33,91,11,0,0),(34,91,55,0,0),(35,91,50,0,0),(36,91,7,0,0),(37,91,117,0,0),(38,91,49,0,0),(39,91,31,0,0),(40,91,176,0,0),(41,91,23,0,0),(42,91,87,0,0),(43,91,8,0,0),(44,91,27,0,0),(45,91,107,0,0),(46,91,30,0,0),(47,91,26,0,0),(48,91,22,0,0),(49,91,116,0,0),(50,91,84,0,0),(51,91,97,0,0),(52,91,37,0,0),(53,91,62,0,0),(54,91,48,0,0),(55,91,57,0,0),(56,91,56,0,0),(57,91,44,0,0),(58,91,174,0,0),(59,91,31,0,0),(60,91,18,0,0),(61,92,46,0,0),(62,92,62,0,0),(63,92,17,0,0),(64,92,14,0,0),(65,92,51,0,0),(66,92,41,0,0),(67,92,22,0,0),(68,92,86,0,0),(69,92,84,0,0),(70,92,32,0,0),(71,92,81,0,0),(72,92,11,0,0),(73,92,53,0,0),(74,92,20,0,0),(75,92,75,0,0),(76,92,17,0,0),(77,92,19,0,0),(78,92,59,0,0),(79,92,27,0,0),(80,92,23,0,0),(81,92,56,0,0),(82,92,103,0,0),(83,92,2,0,0),(84,92,46,0,0),(85,92,43,0,0),(86,92,49,0,0),(87,92,6,0,0),(88,92,12,0,0),(89,92,17,0,0),(90,92,13,0,0),(91,92,48,0,0),(92,92,40,0,0),(93,92,88,0,0),(94,92,110,0,0),(95,92,47,0,0),(96,92,125,0,0),(97,92,20,0,0),(98,92,103,0,0),(99,92,187,0,0),(100,92,13,0,0),(101,92,45,0,0),(102,92,167,0,0),(103,92,190,0,0),(104,92,101,0,0),(105,92,30,0,0),(106,92,173,0,0),(107,92,46,0,0),(108,92,58,0,0),(109,92,34,0,0),(110,92,123,0,0),(111,92,115,0,0),(112,92,88,0,0),(113,92,12,0,0),(114,92,157,0,0),(115,92,53,0,0),(116,92,136,0,0),(117,92,37,0,0),(118,92,88,0,0),(119,92,4,0,0),(120,92,53,0,0),(121,93,54,0,0),(122,93,70,0,0),(123,93,25,0,0),(124,93,76,0,0),(125,93,42,0,0),(126,93,7,0,0),(127,93,24,0,0),(128,93,4,0,0),(129,93,22,0,0),(130,93,11,0,0),(131,93,75,0,0),(132,93,5,0,0),(133,93,53,0,0),(134,93,84,0,0),(135,93,3,0,0),(136,93,29,0,0),(137,93,8,0,0),(138,93,59,0,0),(139,93,49,0,0),(140,93,33,0,0),(141,93,39,0,0),(142,93,60,0,0),(143,93,41,0,0),(144,93,84,0,0),(145,93,119,0,0),(146,93,4,0,0),(147,93,117,0,0),(148,93,77,0,0),(149,93,64,0,0),(150,93,47,0,0),(151,93,22,0,0),(152,93,51,0,0),(153,93,22,0,0),(154,93,79,0,0),(155,93,92,0,0),(156,93,142,0,0),(157,93,13,0,0),(158,93,22,0,0),(159,93,158,0,0),(160,93,43,0,0),(161,93,52,0,0),(162,93,42,0,0),(163,93,36,0,0),(164,93,66,0,0),(165,93,50,0,0),(166,93,179,0,0),(167,93,6,0,0),(168,93,54,0,0),(169,93,55,0,0),(170,93,67,0,0),(171,93,45,0,0),(172,93,45,0,0),(173,93,69,0,0),(174,93,147,0,0),(175,93,18,0,0),(176,93,79,0,0),(177,93,5,0,0),(178,93,9,0,0),(179,93,79,0,0),(180,93,8,0,0),(181,94,73,0,1),(182,94,83,0,0),(183,94,32,0,0),(184,94,15,0,0),(185,94,64,0,0),(186,94,3,0,0),(187,94,54,0,0),(188,94,32,0,0),(189,94,26,0,0),(190,94,13,0,0),(191,94,6,0,0),(192,94,46,0,0),(193,94,1,0,0),(194,94,38,0,0),(195,94,51,0,0),(196,94,56,0,0),(197,94,43,0,0),(198,94,52,0,0),(199,94,56,0,0),(200,94,13,0,0),(201,94,32,0,0),(202,94,15,0,0),(203,94,52,0,0),(204,94,103,0,0),(205,94,26,0,0),(206,94,71,0,0),(207,94,36,0,0),(208,94,10,0,0),(209,94,20,0,0),(210,94,45,0,0),(211,94,39,0,0),(212,94,123,0,0),(213,94,22,0,0),(214,94,11,0,0),(215,94,37,0,0),(216,94,184,0,0),(217,94,88,0,0),(218,94,72,0,0),(219,94,15,0,0),(220,94,55,0,0),(221,94,15,0,0),(222,94,45,0,0),(223,94,57,0,0),(224,94,14,0,0),(225,94,56,0,0),(226,94,45,0,0),(227,94,56,0,0),(228,94,9,0,0),(229,94,51,0,0),(230,94,183,0,0),(231,94,42,0,0),(232,94,158,0,0),(233,94,40,0,0),(234,94,94,0,0),(235,94,173,0,0),(236,94,162,0,0),(237,94,128,0,0),(238,94,147,0,0),(239,94,117,0,0),(240,94,90,0,0),(241,95,45,0,1),(242,96,30,0,0),(243,97,81,0,1),(244,98,37,0,1),(245,99,71,0,1),(246,1,1,0,1),(247,2,43,0,1),(248,3,17,0,0),(249,4,71,0,0),(250,5,58,0,1),(251,6,46,0,0);
/*!40000 ALTER TABLE `test_question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `credentials_expired` tinyint(1) NOT NULL,
  `credentials_expire_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (5,'nico','nico','blaudez@free.Fr','blaudez@free.fr',1,'58qo1wctyvc4000004w4w4gsokks8oo','$2y$13$TPPnSOf/QCihu3wgvdbNZuUmRhZY8tRovSwoPSCO4odSf8p65PPWm','2016-03-20 21:20:12',0,0,NULL,NULL,NULL,'a:0:{}',0,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-21  0:09:10
