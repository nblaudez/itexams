CSS Questions.




1/


Which of this selector will be prioritary ?

X - #header {}
- .header {}
- header  {}


2/

it is possible to imbricate declaration as the following code in CSS 3 ?

header {
	background-color:black;
	color:white;
	
	.sub-header {
		background-color:white;
		color:black;
	}
}

- yes
- no
X - no but it's possible with LESS preprocessor



3 / Which property permits to round the corners of an element ?

- box-shadow
X - border-radius
- round-corner
- it is not possible


4 / how center an element with margin ?

- margin: center;
- margin: textcenter;
X - margin: 0 auto;
- it is not possible


5 / Does this code is a valid CSS code ?


.header, header , footer > sub-footer {
	background-color:black;
	color:blue;
	font-size:14em;
}

- no
X - yes 
- only in CSS 3


6 / What selector permits to select the 3rd li of an ul element with the class choice?

- ul.choice > li{3}
- li.choice[3]
X - ul.choice li:nth-child(3)
- ul li:child(2)


7 / what is the difference between padding and margin ?

- margin is in the element while padding out the element
X - margin is out the element while padding in in the element
- padding do not exists
- margin do not exists
- padding and margin are the same things

8 / how change the background color of an element ?

- color-background : #dedede;
- color: #dedede;
X - background: #00ff00 url("smiley.gif") no-repeat fixed center; 
X - background-color: #dedede;


9 / How align an element to the left ?

- margin : left;
- padding : left;
X - float : left;
- align : left;



10 / how positionate an element that will stay at the same place on the screen ?

- With position static
X - With position fixed
- With position relative
- It is not possible


